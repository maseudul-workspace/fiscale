package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchExecutiveWalletHistoryInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.WalletHistoryData;
import com.webinfotech.fiscale.domain.models.WalletHistoryWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchExecutiveWalletHistoryInteractorImpl extends AbstractInteractor implements FetchExecutiveWalletHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int memberId;
    int page;

    public FetchExecutiveWalletHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int memberId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.memberId = memberId;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(WalletHistoryData walletHistoryData, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistorySuccess(walletHistoryData, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final WalletHistoryWrapper walletHistoryWrapper = mRepository.fetchExecutiveWalletHistory(apiToken, memberId, page);
        if (walletHistoryWrapper == null) {
            notifyError("Something Went Wrong", walletHistoryWrapper.login_error);
        } else if(!walletHistoryWrapper.status) {
            notifyError(walletHistoryWrapper.message, walletHistoryWrapper.login_error);
        } else {
            postMessage(walletHistoryWrapper.walletHistoryData, walletHistoryWrapper.totalPage);
        }
    }
}
