package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchSpWalletHistoryInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.WalletHistoryData;
import com.webinfotech.fiscale.domain.models.WalletHistoryWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchSpWalletHistoryInteractorImpl extends AbstractInteractor implements FetchSpWalletHistoryInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int spId;
    String apiToken;

    public FetchSpWalletHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int spId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.spId = spId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWalletFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(WalletHistoryData walletHistoryData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWalletFetchSuccess(walletHistoryData);
            }
        });
    }

    @Override
    public void run() {
        final WalletHistoryWrapper walletHistoryWrapper = mRepository.fetchSpWalletHistory(spId, apiToken);
        if (walletHistoryWrapper == null) {
            notifyError("Something Went Wrong", walletHistoryWrapper.login_error);
        } else if (!walletHistoryWrapper.status) {
            notifyError(walletHistoryWrapper.message, walletHistoryWrapper.login_error);
        } else {
            postMessage(walletHistoryWrapper.walletHistoryData);
        }
    }
}
