package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.JobDescList;

public interface GetJobDescListInteractor {
    interface Callback {
        void onGettingJobDescListSuccess(JobDescList[] jobDescLists);
        void onGettingJobDescListFail(String errorMsg);
    }
}
