package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.MemberClientDetails;

public interface GetMemberClientDetailsInteractor {
    interface Callback {
        void onGettingClientDetailsSuccess(MemberClientDetails clientDetails);
        void onGettingClientDetailsFail(String errorMsg, int loginError);
    }
}
