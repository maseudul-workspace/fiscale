package com.webinfotech.fiscale.domain.models.Testing;

public class MainJobs {

    public String date;
    public String jobDesc;
    public String status;
    public String clientName;

    public MainJobs(String date, String jobDesc, String status, String clientName) {
        this.date = date;
        this.jobDesc = jobDesc;
        this.status = status;
        this.clientName = clientName;
    }

}
