package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Remarks {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("job_id")
    @Expose
    public int jobId;

    @SerializedName("remarks")
    @Expose
    public String remarks;

    @SerializedName("remarks_by_name")
    @Expose
    public String remarksByName;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

}
