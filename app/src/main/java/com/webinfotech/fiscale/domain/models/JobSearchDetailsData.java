package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobSearchDetailsData {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("client_id")
    @Expose
    public int clientId;

    @SerializedName("job_type")
    @Expose
    public int jobTypeId;

    @SerializedName("job_id")
    @Expose
    public String jobId;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @SerializedName("job_type_name")
    @Expose
    public String jobTypeName;

    @SerializedName("dob")
    @Expose
    public String dob;

    @SerializedName("constitution")
    @Expose
    public String constitution;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("description")
    @Expose
    public String jobDesc;

    @SerializedName("cl_name")
    @Expose
    public String clientName;

    @SerializedName("cl_pan")
    @Expose
    public String clientPan;

    @SerializedName("cl_mobile")
    @Expose
    public String clientMobile;

    @SerializedName("comments")
    @Expose
    public Remarks[] remarks;

}
