package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.SearchClientJobDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.SearchClientWithJobsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsDataWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class SearchClientJobDetailsInteractorImpl extends AbstractInteractor implements SearchClientJobDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String jobId;

    public SearchClientJobDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String jobId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.jobId = jobId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClientJobDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(JobSearchDetailsData jobSearchDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClientJobDetailsSuccess(jobSearchDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final JobSearchDetailsDataWrapper jobSearchDetailsDataWrapper = mRepository.searchClientJobs(apiToken, jobId);
        if (jobSearchDetailsDataWrapper == null) {
            notifyError("Something Went Wrong", jobSearchDetailsDataWrapper.login_error);
        } else if (!jobSearchDetailsDataWrapper.status) {
            notifyError(jobSearchDetailsDataWrapper.message, jobSearchDetailsDataWrapper.login_error);
        } else {
            postMessage(jobSearchDetailsDataWrapper.jobSearchDetailsData);
        }
    }
}
