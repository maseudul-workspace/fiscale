package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetClientListInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.ClientListWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class GetClientListInteractorImpl extends AbstractInteractor implements GetClientListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int spId;
    int page;

    public GetClientListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int spId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.spId = spId;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClientListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ClientList[] clientLists, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClientListSuccess(clientLists, page);
            }
        });
    }


    @Override
    public void run() {
        final ClientListWrapper clientListWrapper = mRepository.getClientList(apiToken, spId, page);
        if (clientListWrapper == null) {
            notifyError("Something Went Wrong", clientListWrapper.login_error);
        } else if (!clientListWrapper.status) {
            notifyError(clientListWrapper.message, clientListWrapper.login_error);
        } else {
            postMessage(clientListWrapper.clients, clientListWrapper.totalPage);
        }
    }
}
