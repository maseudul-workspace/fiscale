package com.webinfotech.fiscale.domain.interactors;

public interface AddClientExistingJobInteractor {
    interface Callback {
        void onAddExistingJobSuccess();
        void onAddExistinJobFail(String errorMsg, int loginError);
    }
}
