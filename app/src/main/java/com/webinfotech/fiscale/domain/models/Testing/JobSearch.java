package com.webinfotech.fiscale.domain.models.Testing;

public class JobSearch {

    public String date;
    public String commentedBy;
    public String remarks;

    public JobSearch(String date, String commentedBy, String remarks) {
        this.date = date;
        this.commentedBy = commentedBy;
        this.remarks = remarks;
    }
}
