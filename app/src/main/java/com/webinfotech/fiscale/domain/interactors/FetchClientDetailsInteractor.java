package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.ClientDetails;

public interface FetchClientDetailsInteractor {
    interface Callback {
        void onFetchingClientDetailsSuccess(ClientDetails clientDetails);
        void onFetchingClientDetailsFail(String errorMsg, int loginError);
    }
}
