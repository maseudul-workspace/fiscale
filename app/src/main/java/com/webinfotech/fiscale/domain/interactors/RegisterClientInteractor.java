package com.webinfotech.fiscale.domain.interactors;

public interface RegisterClientInteractor {
    interface Callback {
        void onRegisterClientSuccess();
        void onRegisterClientFail(String errorMsg);
    }
}
