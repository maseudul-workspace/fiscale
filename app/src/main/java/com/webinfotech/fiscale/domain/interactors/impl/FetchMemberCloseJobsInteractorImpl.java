package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchMemberCloseJobsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.CloseJobs;
import com.webinfotech.fiscale.domain.models.CloseJobsWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchMemberCloseJobsInteractorImpl extends AbstractInteractor implements FetchMemberCloseJobsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int memberId;
    int pageNo;

    public FetchMemberCloseJobsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String apiToken, int memberId, int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.memberId = memberId;
        this.pageNo = pageNo;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCloseJobFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(CloseJobs[] openJobs, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCloseJobsSuccess(openJobs, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final CloseJobsWrapper closeJobsWrapper = mRepository.fetchMemberCloseJobs(apiToken, memberId, pageNo);
        if (closeJobsWrapper == null) {
            notifyError("Something Went Wrong", closeJobsWrapper.login_error);
        } else if (!closeJobsWrapper.status) {
            notifyError(closeJobsWrapper.message, closeJobsWrapper.login_error);
        } else {
            postMessage(closeJobsWrapper.closeJobs, closeJobsWrapper.totalPage);
        }
    }
}
