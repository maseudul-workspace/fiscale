package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetMemberClientDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.ClientDetailsWrapper;
import com.webinfotech.fiscale.domain.models.MemberClientDetails;
import com.webinfotech.fiscale.domain.models.MemberClientDetailsWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class GetMemberClientDetailsInteractorImpl extends AbstractInteractor implements GetMemberClientDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;


    public GetMemberClientDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClientDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(MemberClientDetails clientDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClientDetailsSuccess(clientDetails);
            }
        });
    }

    @Override
    public void run() {
        final MemberClientDetailsWrapper clientDetailsWrapper = mRepository.getMemberClientDetails(apiToken, userId);
        if (clientDetailsWrapper == null) {
            notifyError("Something Went Wrong", clientDetailsWrapper.login_error);
        } else if (!clientDetailsWrapper.status) {
            notifyError(clientDetailsWrapper.message, clientDetailsWrapper.login_error);
        } else {
            postMessage(clientDetailsWrapper.memberClientDetails);
        }
    }
}
