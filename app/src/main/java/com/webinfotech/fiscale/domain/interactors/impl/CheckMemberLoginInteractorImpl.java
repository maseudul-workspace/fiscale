package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.CheckMemberLoginInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.domain.models.UserInfoWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class CheckMemberLoginInteractorImpl extends AbstractInteractor implements CheckMemberLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckMemberLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFailed(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.checkMemberLogin(email, password);
        if (userInfoWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message);
        } else {
            userInfoWrapper.userInfo.userType = 2;
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
