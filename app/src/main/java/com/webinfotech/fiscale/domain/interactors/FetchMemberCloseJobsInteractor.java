package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.CloseJobs;

public interface FetchMemberCloseJobsInteractor {
    interface Callback {
        void onGettingCloseJobsSuccess(CloseJobs[] closeJobs, int totalPage);
        void onGettingCloseJobFail(String errorMsg, int loginError);
    }
}
