package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.SearchMemberClientInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.ClientSearchData;
import com.webinfotech.fiscale.domain.models.ClientSearchWrapper;
import com.webinfotech.fiscale.domain.models.SearchClientListWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class SearchMemberClientInteractorImpl extends AbstractInteractor implements SearchMemberClientInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String searchKey;

    public SearchMemberClientInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String searchKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.searchKey = searchKey;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSearchClientFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ClientSearchData clientSearchData) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSearchClientSuccess(clientSearchData);
            }
        });
    }

    @Override
    public void run() {
        final ClientSearchWrapper clientSearchWrapper = mRepository.searchMemberClient(apiToken, searchKey);
        if (clientSearchWrapper == null) {
            notifyError("Something Went Wrong", clientSearchWrapper.login_error);
        } else if (!clientSearchWrapper.status) {
            notifyError(clientSearchWrapper.message, clientSearchWrapper.login_error);
        } else {
            postMessage(clientSearchWrapper.clientSearchData);
        }
    }
}
