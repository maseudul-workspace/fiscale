package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.UserInfo;

public interface CheckExecutiveLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFailed(String errorMsg);
    }
}
