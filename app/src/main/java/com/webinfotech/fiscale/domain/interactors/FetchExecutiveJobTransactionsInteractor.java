package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.TransactionJob;

public interface FetchExecutiveJobTransactionsInteractor {
    interface Callback {
        void onGettingJobTransactionsSuccess(TransactionJob[] transactionJobs, int totalPage);
        void onGettingJobTransactionsFail(String errorMsg, int loginError);
    }
}
