package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpenJobs {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("job_id")
    @Expose
    public String jobId;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("c_name")
    @Expose
    public String clientName;

    @SerializedName("pan")
    @Expose
    public String pan;

    @SerializedName("job_type_name")
    @Expose
    public String jobTypeName;

    @SerializedName("employee_name")
    @Expose
    public String employeeName;

    @SerializedName("branch_name")
    @Expose
    public String branch_name;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

}
