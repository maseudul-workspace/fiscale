package com.webinfotech.fiscale.domain.interactors;

public interface UpdateMemberJobInteractor {
    interface Callback {
        void onUpdateJobSuccess();
        void onUpdateJobFail(String errorMsg, int loginError);
    }
}
