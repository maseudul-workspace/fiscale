package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;

public interface FetchMemberJobDetailsInteractor {
    interface Callback {
        void onGettingMemberJobDetailsSuccess(JobSearchDetailsData jobSearchDetailsData);
        void onGettingMemberJobDetailsFail(String errorMsg, int loginError);
    }
}
