package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.AddClientExistingJobInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.CommonResponse;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class AddClientExistingJobInteractorImpl extends AbstractInteractor implements AddClientExistingJobInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    ArrayList<Integer> jobIds;
    int clientId;
    int spId;

    public AddClientExistingJobInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, ArrayList<Integer> jobIds, int clientId, int spId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.jobIds = jobIds;
        this.clientId = clientId;
        this.spId = spId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddExistinJobFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddExistingJobSuccess();
            }
        });
    }


    @Override
    public void run() {
       final CommonResponse commonResponse = mRepository.addExistingClientJobs(apiToken, jobIds, clientId, spId);
       if (commonResponse == null) {
           notifyError("Something Went Wrong", commonResponse.login_error);
       } else if (!commonResponse.status) {
           notifyError(commonResponse.message, commonResponse.login_error);
       } else {
           postMessage();
       }
    }
}
