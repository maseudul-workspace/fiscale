package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchJobTransactionsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.TransactionJob;
import com.webinfotech.fiscale.domain.models.TransactionJobWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchJobTransactionsInteractorImpl extends AbstractInteractor implements FetchJobTransactionsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String startDate;
    String endDate;
    int memberId;
    int page;

    public FetchJobTransactionsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String startDate, String endDate, int memberId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.startDate = startDate;
        this.endDate = endDate;
        this.memberId = memberId;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobTransactionsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(TransactionJob[] transactionJobs, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobTransactionsSuccess(transactionJobs, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final TransactionJobWrapper transactionJobWrapper = mRepository.fetchTransactionJobs(apiToken, startDate, endDate, memberId, page);
        if (transactionJobWrapper == null) {
            notifyError("Something Went Wrong", transactionJobWrapper.login_error);
        } else if (!transactionJobWrapper.status) {
            notifyError(transactionJobWrapper.message, transactionJobWrapper.login_error);
        } else {
            postMessage(transactionJobWrapper.transactionJobs, transactionJobWrapper.totalPage);
        }
    }
}
