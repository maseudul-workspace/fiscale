package com.webinfotech.fiscale.domain.interactors;

public interface RejectJobInteractor {
    interface Callback {
        void onRejectJobSuccess();
        void onRejectJobFail(String errorMsg, int loginError);
    }
}
