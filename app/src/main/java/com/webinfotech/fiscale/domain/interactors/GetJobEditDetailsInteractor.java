package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;

public interface GetJobEditDetailsInteractor {
    interface Callback {
        void onGettingJobEditDetailsSuccess(JobSearchDetailsData jobSearchDetailsData);
        void  onGettingJobEditDetailsFail(String errorMsg, int loginError);
    }
}
