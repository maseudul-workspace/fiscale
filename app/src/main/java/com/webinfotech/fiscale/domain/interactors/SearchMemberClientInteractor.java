package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.ClientSearchData;

public interface SearchMemberClientInteractor {
    interface Callback {
        void onSearchClientSuccess(ClientSearchData clientSearchData);
        void onSearchClientFail(String errorMsg, int loginError);
    }
}
