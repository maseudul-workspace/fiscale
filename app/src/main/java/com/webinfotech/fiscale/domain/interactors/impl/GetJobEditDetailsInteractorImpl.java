package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetJobEditDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsDataWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class GetJobEditDetailsInteractorImpl extends AbstractInteractor implements GetJobEditDetailsInteractor {

    AppRepositoryImpl mRepository;
    GetJobEditDetailsInteractor.Callback mCallback;
    String apiToken;
    int jobId;

    public GetJobEditDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int jobId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.jobId = jobId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobEditDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(JobSearchDetailsData jobSearchDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobEditDetailsSuccess(jobSearchDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final JobSearchDetailsDataWrapper jobSearchDetailsDataWrapper = mRepository.getJobEditDetails(apiToken, jobId);
        if (jobSearchDetailsDataWrapper == null) {
            notifyError("Something Went Wrong", jobSearchDetailsDataWrapper.login_error);
        } else if (!jobSearchDetailsDataWrapper.status) {
            notifyError(jobSearchDetailsDataWrapper.message, jobSearchDetailsDataWrapper.login_error);
        } else {
            postMessage(jobSearchDetailsDataWrapper.jobSearchDetailsData);
        }
    }
}
