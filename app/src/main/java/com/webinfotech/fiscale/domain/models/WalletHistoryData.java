package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistoryData {

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("wallet_history")
    @Expose
    public WalletHistory[] walletHistories;

}
