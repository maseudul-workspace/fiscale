package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("flat_no")
    @Expose
    public String flatNo;

    @SerializedName("village")
    @Expose
    public String village;

    @SerializedName("po")
    @Expose
    public String po;

    @SerializedName("ps")
    @Expose
    public String ps;

    @SerializedName("area")
    @Expose
    public String area;

    @SerializedName("dist")
    @Expose
    public String dist;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("pin")
    @Expose
    public String pin;

}
