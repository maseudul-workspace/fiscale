package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.OpenJobs;

public interface FetchOpenJobsInteractor {
    interface Callback {
        void onGettingOpenJobsSuccess(OpenJobs[] openJobs, int totalPage);
        void onGettingOpenJobFail(String errorMsg, int loginError);
    }
}
