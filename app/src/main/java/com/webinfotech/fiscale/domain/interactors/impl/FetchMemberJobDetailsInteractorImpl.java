package com.webinfotech.fiscale.domain.interactors.impl;

import android.content.Context;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchMemberJobDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsDataWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchMemberJobDetailsInteractorImpl extends AbstractInteractor implements FetchMemberJobDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String jobId;

    public FetchMemberJobDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, Context mContext, AppRepositoryImpl mRepository, Callback callback, String apiToken, String jobId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.jobId = jobId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMemberJobDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(JobSearchDetailsData jobSearchDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMemberJobDetailsSuccess(jobSearchDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final JobSearchDetailsDataWrapper jobSearchDetailsDataWrapper = mRepository.fetchMemberJobDetails(apiToken, jobId);
        if (jobSearchDetailsDataWrapper == null) {
            notifyError("Something Went Wrong", jobSearchDetailsDataWrapper.login_error);
        } else if (!jobSearchDetailsDataWrapper.status) {
            notifyError(jobSearchDetailsDataWrapper.message, jobSearchDetailsDataWrapper.login_error);
        } else {
            postMessage(jobSearchDetailsDataWrapper.jobSearchDetailsData);
        }
    }
}
