package com.webinfotech.fiscale.domain.models.Testing;

public class ClosedJobs {

    public String date;
    public String jobDesc;
    public String status;
    public String clientName;
    public String closedDate;

    public ClosedJobs(String date, String jobDesc, String status, String clientName, String closedDate) {
        this.date = date;
        this.jobDesc = jobDesc;
        this.status = status;
        this.clientName = clientName;
        this.closedDate = closedDate;
    }

}
