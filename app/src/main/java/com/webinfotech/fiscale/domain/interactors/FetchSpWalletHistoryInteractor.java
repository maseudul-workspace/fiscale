package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.WalletHistoryData;

public interface FetchSpWalletHistoryInteractor {
    interface Callback {
        void onWalletFetchSuccess(WalletHistoryData walletHistoryData);
        void onWalletFetchFail(String errorMsg, int loginError);
    }
}
