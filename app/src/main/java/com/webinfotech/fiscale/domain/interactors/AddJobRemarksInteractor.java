package com.webinfotech.fiscale.domain.interactors;

public interface AddJobRemarksInteractor {
    interface Callback {
        void onJobRemarksAddSuccess();
        void onJobRemarksAddFail(String errorMsg, int loginError);
    }
}
