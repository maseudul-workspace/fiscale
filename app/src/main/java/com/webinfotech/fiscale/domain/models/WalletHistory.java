package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("wallet_id")
    @Expose
    public int walletId;

    @SerializedName("transaction_type")
    @Expose
    public int transactionType;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("balance")
    @Expose
    public String balance;

    @SerializedName("total_amount")
    @Expose
    public String totalAmount;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

}
