package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.RegisterClientInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.CommonResponse;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class RegisterClientInteractorImpl extends AbstractInteractor implements RegisterClientInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String name;
    String email;
    String mobile;
    String pan;
    String fatherName;
    String dob;
    String gender;
    String constitution;
    String tradeName;
    int createdById;
    String flatNo;
    String village;
    String po;
    String ps;
    String area;
    String district;
    String state;
    String pin;
    String flatNoBusiness;
    String villageBusiness;
    String poBusiness;
    String psBusiness;
    String areaBusiness;
    String districtBusiness;
    String stateBusiness;
    String pinBusiness;
    ArrayList<Integer> jobIds;

    public RegisterClientInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String name, String email, String mobile, String pan, String fatherName, String dob, String gender, String constitution, String tradeName, int createdById, String flatNo, String village, String po, String ps, String area, String district, String state, String pin, String flatNoBusiness, String villageBusiness, String poBusiness, String psBusiness, String areaBusiness, String districtBusiness, String stateBusiness, String pinBusiness, ArrayList<Integer> jobIds) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.pan = pan;
        this.fatherName = fatherName;
        this.dob = dob;
        this.gender = gender;
        this.constitution = constitution;
        this.tradeName = tradeName;
        this.createdById = createdById;
        this.flatNo = flatNo;
        this.village = village;
        this.po = po;
        this.ps = ps;
        this.area = area;
        this.district = district;
        this.state = state;
        this.pin = pin;
        this.flatNoBusiness = flatNoBusiness;
        this.villageBusiness = villageBusiness;
        this.poBusiness = poBusiness;
        this.psBusiness = psBusiness;
        this.areaBusiness = areaBusiness;
        this.districtBusiness = districtBusiness;
        this.stateBusiness = stateBusiness;
        this.pinBusiness = pinBusiness;
        this.jobIds = jobIds;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterClientFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterClientSuccess();
            }
        });
    }


    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.registerClient(apiToken,
                                                                        name,
                                                                        email,
                                                                        mobile,
                                                                        pan,
                                                                        fatherName,
                                                                        dob,
                                                                        gender,
                                                                        constitution,
                                                                        tradeName,
                                                                        createdById,
                                                                        flatNo,
                                                                        village,
                                                                        po,
                                                                        ps,
                                                                        area,
                                                                        district,
                                                                        state,
                                                                        pin,
                                                                        flatNoBusiness,
                                                                        villageBusiness,
                                                                        poBusiness,
                                                                        psBusiness,
                                                                        areaBusiness,
                                                                        districtBusiness,
                                                                        stateBusiness,
                                                                        pinBusiness,
                                                                        jobIds
                );
        if (commonResponse == null) {
            notifyError("Something went wrong");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
