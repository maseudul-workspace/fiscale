package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("branch_id")
    @Expose
    public String branchId;

    @SerializedName("executive_id")
    @Expose
    public int executiveId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("designation")
    @Expose
    public String designation;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("api_token")
    @Expose
    public String api_token;

    public int userType;

    @SerializedName("status")
    @Expose
    public int status;

}
