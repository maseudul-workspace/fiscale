package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.ClientList;

public interface GetClientListInteractor {
    interface Callback {
        void onGettingClientListSuccess(ClientList[] clientLists, int totalPage);
        void onGettingClientListFail(String errorMsg, int loginError);
    }
}
