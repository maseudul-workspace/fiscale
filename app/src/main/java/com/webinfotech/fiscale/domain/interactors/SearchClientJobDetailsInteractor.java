package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;

public interface SearchClientJobDetailsInteractor {
    interface Callback {
        void onGettingClientJobDetailsSuccess(JobSearchDetailsData jobSearchDetailsData);
        void onGettingClientJobDetailsFail(String errorMsg, int loginError);
    }
}
