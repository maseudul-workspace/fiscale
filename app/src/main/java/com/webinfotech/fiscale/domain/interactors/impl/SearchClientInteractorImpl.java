package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.SearchClientInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.ClientListWrapper;
import com.webinfotech.fiscale.domain.models.SearchClientListWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class SearchClientInteractorImpl extends AbstractInteractor implements SearchClientInteractor
{

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String searchKey;

    public SearchClientInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String searchKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.searchKey = searchKey;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSearchClientFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ClientList clientLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSearchClientSuccess(clientLists);
            }
        });
    }

    @Override
    public void run() {
        final SearchClientListWrapper searchClientListWrapper = mRepository.searchClient(apiToken, searchKey);
        if (searchClientListWrapper == null) {
            notifyError("Something Went Wrong", searchClientListWrapper.login_error);
        } else if (!searchClientListWrapper.status) {
            notifyError(searchClientListWrapper.message, searchClientListWrapper.login_error);
        } else {
           postMessage(searchClientListWrapper.clientList);
        }
    }
}
