package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("message")
    @Expose
    public String message;

}
