package com.webinfotech.fiscale.domain.interactors;

public interface UpdateClientInteractor {
    interface Callback {
        void onUpdateUserSuccess();
        void onUpdateUserFail(String errorMsg, int loginError);
    }
}
