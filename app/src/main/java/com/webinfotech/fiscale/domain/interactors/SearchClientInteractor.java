package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.ClientList;

public interface SearchClientInteractor {
    interface Callback {
        void onSearchClientSuccess(ClientList clientList);
        void onSearchClientFail(String errorMsg, int loginError);
    }
}
