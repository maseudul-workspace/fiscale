package com.webinfotech.fiscale.domain.interactors.impl;

import android.content.Context;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetJobDescListInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.domain.models.JobDescListWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class GetJobDescListInteractorImpl extends AbstractInteractor implements GetJobDescListInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public GetJobDescListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobDescListFail(errorMsg);
            }
        });
    }

    private void postMessage(JobDescList[] jobDescLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobDescListSuccess(jobDescLists);
            }
        });
    }


    @Override
    public void run() {
        JobDescListWrapper jobDescListWrapper = mRepository.fetchJobDescList();
        if (jobDescListWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!jobDescListWrapper.status) {
            notifyError(jobDescListWrapper.message);
        } else {
            postMessage(jobDescListWrapper.jobDescList);
        }
    }
}
