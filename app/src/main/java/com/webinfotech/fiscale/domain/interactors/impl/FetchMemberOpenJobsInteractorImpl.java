package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.OpenJobs;
import com.webinfotech.fiscale.domain.models.OpenJobsWrapper;
import com.webinfotech.fiscale.presentation.presenters.FetchMemberOpenJobsInteractor;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchMemberOpenJobsInteractorImpl extends AbstractInteractor implements FetchMemberOpenJobsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int memberId;
    int pageNo;

    public FetchMemberOpenJobsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String apiToken, int memberId, int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.memberId = memberId;
        this.pageNo = pageNo;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMemberOpenJobsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OpenJobs[] openJobs, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMemberOpenJobsSuccess(openJobs, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final OpenJobsWrapper openJobsWrapper = mRepository.fetchMemberOpenJobs(apiToken, memberId, pageNo);
        if (openJobsWrapper == null) {
            notifyError("Something Went Wrong", openJobsWrapper.login_error);
        } else if (!openJobsWrapper.status) {
            notifyError(openJobsWrapper.message, openJobsWrapper.login_error);
        } else {
            postMessage(openJobsWrapper.openJobs, openJobsWrapper.totalPage);
        }
    }
}
