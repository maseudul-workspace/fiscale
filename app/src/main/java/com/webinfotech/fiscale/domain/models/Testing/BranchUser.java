package com.webinfotech.fiscale.domain.models.Testing;

public class BranchUser {

    public String name;
    public String pan;
    public String mobile;

    public BranchUser(String name, String pan, String mobile) {
        this.name = name;
        this.pan = pan;
        this.mobile = mobile;
    }

}
