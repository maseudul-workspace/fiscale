package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("job_id")
    @Expose
    public String jobId;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @SerializedName("status")
    @Expose
    public int status;

}
