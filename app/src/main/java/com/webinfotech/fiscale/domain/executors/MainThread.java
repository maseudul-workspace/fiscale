package com.webinfotech.fiscale.domain.executors;

/**
 * Created by Raj on 03-01-2019.
 */

public interface MainThread {
    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
