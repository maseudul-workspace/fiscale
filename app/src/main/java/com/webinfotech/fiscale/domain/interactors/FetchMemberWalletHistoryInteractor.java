package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.WalletHistory;
import com.webinfotech.fiscale.domain.models.WalletHistoryData;

public interface FetchMemberWalletHistoryInteractor {
    interface Callback {
        void onGettingWalletHistorySuccess(WalletHistoryData walletHistoryData, int totalPage);
        void onGettingWalletHistoryFail(String errorMsg, int loginError);
    }
}
