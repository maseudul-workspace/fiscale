package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionJob {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("employee_id")
    @Expose
    public int employeeId;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("job_id")
    @Expose
    public String jobId;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @SerializedName("job_name")
    @Expose
    public String jobName;

}
