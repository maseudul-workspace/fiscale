package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.ClientSearchData;

public interface SearchClientWithJobsInteractor {
    interface Callback {
        void onSearchClientWithJobsSuccess(ClientSearchData clientSearchData);
        void onSearchClientWithJobsFail(String errorMsg, int loginError);
    }
}
