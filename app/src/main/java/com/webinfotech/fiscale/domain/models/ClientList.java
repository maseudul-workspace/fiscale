package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("client_id")
    @Expose
    public int clientId;

    @SerializedName("residential_addr_id")
    @Expose
    public int residentialAddressId;

    @SerializedName("business_addr_id")
    @Expose
    public int businessAddressId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("pan")
    @Expose
    public String pan;

    @SerializedName("father_name")
    @Expose
    public String fatherName;

    @SerializedName("dob")
    @Expose
    public String dob;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("constitution")
    @Expose
    public String constitution;

    @SerializedName("trade_name")
    @Expose
    public String tradeName;

    @SerializedName("status")
    @Expose
    public int status;

}
