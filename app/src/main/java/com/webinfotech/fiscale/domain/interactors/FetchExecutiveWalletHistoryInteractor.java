package com.webinfotech.fiscale.domain.interactors;

import com.webinfotech.fiscale.domain.models.WalletHistoryData;

public interface FetchExecutiveWalletHistoryInteractor {
    interface Callback {
        void onGettingWalletHistorySuccess(WalletHistoryData walletHistoryData, int totalPage);
        void onGettingWalletHistoryFail(String errorMsg, int loginError);
    }
}
