package com.webinfotech.fiscale.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CloseJobsWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("data")
    @Expose
    public CloseJobs[] closeJobs;
}
