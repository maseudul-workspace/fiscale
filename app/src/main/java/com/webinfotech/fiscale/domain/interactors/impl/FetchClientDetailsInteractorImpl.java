package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchClientDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.ClientDetailsWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class FetchClientDetailsInteractorImpl extends AbstractInteractor implements FetchClientDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchClientDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingClientDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ClientDetails clientDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingClientDetailsSuccess(clientDetails);
            }
        });
    }

    @Override
    public void run() {
        final ClientDetailsWrapper clientDetailsWrapper = mRepository.getClientDetails(apiToken, userId);
        if (clientDetailsWrapper == null) {
            notifyError("Something Went Wrong", clientDetailsWrapper.login_error);
        } else if (!clientDetailsWrapper.status) {
            notifyError(clientDetailsWrapper.message, clientDetailsWrapper.login_error);
        } else {
            postMessage(clientDetailsWrapper.clientDetails);
        }
    }
}
