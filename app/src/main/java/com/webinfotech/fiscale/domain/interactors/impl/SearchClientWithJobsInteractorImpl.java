package com.webinfotech.fiscale.domain.interactors.impl;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.SearchClientInteractor;
import com.webinfotech.fiscale.domain.interactors.SearchClientWithJobsInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.ClientSearchData;
import com.webinfotech.fiscale.domain.models.ClientSearchWrapper;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class SearchClientWithJobsInteractorImpl extends AbstractInteractor implements SearchClientWithJobsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String searchKey;

    public SearchClientWithJobsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String searchKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.searchKey = searchKey;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSearchClientWithJobsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ClientSearchData clientSearchData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSearchClientWithJobsSuccess(clientSearchData);
            }
        });
    }

    @Override
    public void run() {
        final ClientSearchWrapper clientSearchWrapper = mRepository.fetchClientWithJobs(apiToken, searchKey);
        if (clientSearchWrapper == null) {
            notifyError("Something Went Wrong", clientSearchWrapper.login_error);
        } else if (!clientSearchWrapper.status) {
            notifyError(clientSearchWrapper.message, clientSearchWrapper.login_error);
        } else {
            postMessage(clientSearchWrapper.clientSearchData);
        }
    }
}
