package com.webinfotech.fiscale.domain.interactors.impl;

import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.UpdateClientInteractor;
import com.webinfotech.fiscale.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fiscale.domain.models.CommonResponse;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class UpdateClientInteractorImpl extends AbstractInteractor implements UpdateClientInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int id;
    int residentialAddressId;
    int businessAddressId;
    String name;
    String email;
    String mobile;
    String pan;
    String fatherName;
    String dob;
    String gender;
    String constitution;
    String tradeName;
    String flatNo;
    String village;
    String po;
    String ps;
    String area;
    String district;
    String state;
    String pin;
    String flatNoBusiness;
    String villageBusiness;
    String poBusiness;
    String psBusiness;
    String areaBusiness;
    String districtBusiness;
    String stateBusiness;
    String pinBusiness;

    public UpdateClientInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int id, int residentialAddressId, int businessAddressId, String name, String email, String mobile, String pan, String fatherName, String dob, String gender, String constitution, String tradeName, String flatNo, String village, String po, String ps, String area, String district, String state, String pin, String flatNoBusiness, String villageBusiness, String poBusiness, String psBusiness, String areaBusiness, String districtBusiness, String stateBusiness, String pinBusiness) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.id = id;
        this.residentialAddressId = residentialAddressId;
        this.businessAddressId = businessAddressId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.pan = pan;
        this.fatherName = fatherName;
        this.dob = dob;
        this.gender = gender;
        this.constitution = constitution;
        this.tradeName = tradeName;
        this.flatNo = flatNo;
        this.village = village;
        this.po = po;
        this.ps = ps;
        this.area = area;
        this.district = district;
        this.state = state;
        this.pin = pin;
        this.flatNoBusiness = flatNoBusiness;
        this.villageBusiness = villageBusiness;
        this.poBusiness = poBusiness;
        this.psBusiness = psBusiness;
        this.areaBusiness = areaBusiness;
        this.districtBusiness = districtBusiness;
        this.stateBusiness = stateBusiness;
        this.pinBusiness = pinBusiness;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateUserFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateUserSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateClient(apiToken, id, residentialAddressId, businessAddressId, name, email, mobile, pan, fatherName, dob, gender, constitution, tradeName, flatNo, village, po, ps, area, district, state, pin, flatNoBusiness, villageBusiness, poBusiness, psBusiness, areaBusiness, districtBusiness, stateBusiness, pinBusiness);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.login_error);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
