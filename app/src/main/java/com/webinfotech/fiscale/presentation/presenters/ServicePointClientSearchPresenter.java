package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.ClientSearchData;
import com.webinfotech.fiscale.presentation.ui.adapters.SpClientSearchJobListAdapter;

public interface ServicePointClientSearchPresenter {
    void searchClientWithJobs(String searchKey);
    interface View {
        void loadClientData(ClientSearchData clientSearchData, SpClientSearchJobListAdapter adapter);
        void onGettingDataFailed();
        void goToJobDetailsActivity(String jobId, int id);
        void showLoader();
        void hideLoader();
    }
}
