package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.presentation.presenters.ExecutiveJobTransactionPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.ExecutiveJobTransactionPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.JobTransactionsAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.Calendar;

public class ExecutiveJobTransactionActivity extends ExecutiveBaseActivity implements ExecutiveJobTransactionPresenter.View {

    @BindView(R.id.recycler_view_job_transactions)
    RecyclerView recyclerView;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    boolean isStartDateSelected = false;
    boolean isEndDateSelected = false;
    String startDate;
    String endDate;
    ExecutiveJobTransactionPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_executive_job_transaction);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ExecutiveJobTransactionPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.layout_start_date) void onStartDateLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        isStartDateSelected = true;
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.layout_end_date) void onEndDateLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        isEndDateSelected = true;
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void loadAdapter(JobTransactionsAdapter adapter, int totalPage) {
        mainLayout.setVisibility(View.VISIBLE);
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchJobTransactions(startDate, endDate, pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void onDataFetchFailed() {
        mainLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (isStartDateSelected && isEndDateSelected) {
            mPresenter.fetchJobTransactions(startDate, endDate, pageNo, "refresh");
        } else {
            Toasty.warning(this, "Please select dates", Toast.LENGTH_SHORT).show();
        }
    }
}
