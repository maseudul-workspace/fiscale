package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.WalletHistory;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {

    Context mContext;
    WalletHistory[] walletHistories;

    public WalletHistoryAdapter(Context mContext, WalletHistory[] walletHistories) {
        this.mContext = mContext;
        this.walletHistories = walletHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_wallet_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        if (walletHistories[position].transactionType == 1) {
            holder.txtViewTransactionType.setText("Debit");
            holder.txtViewTransactionType.setTextColor(mContext.getResources().getColor(R.color.md_red_600));
        } else {
            holder.txtViewTransactionType.setText("Credit");
            holder.txtViewTransactionType.setTextColor(mContext.getResources().getColor(R.color.green2));
        }
        holder.txtViewComments.setText(walletHistories[position].comment);
        holder.txtViewAmount.setText("₹ " + walletHistories[position].amount);
        if (walletHistories[position].balance != null) {
            holder.txtViewTotalBalance.setText("₹ " + walletHistories[position].balance);
        }
        if (walletHistories[position].totalAmount != null) {
            holder.txtViewTotalBalance.setText("₹ " + walletHistories[position].totalAmount);
        }
        holder.txtViewDate.setText(walletHistories[position].createdAt);
    }

    @Override
    public int getItemCount() {
        return walletHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_transaction_type)
        TextView txtViewTransactionType;
        @BindView(R.id.txt_view_comments)
        TextView txtViewComments;
        @BindView(R.id.txt_view_amount)
        TextView txtViewAmount;
        @BindView(R.id.txt_view_total_balance)
        TextView txtViewTotalBalance;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(WalletHistory[] walletHistories) {
        this.walletHistories = walletHistories;
    }

}
