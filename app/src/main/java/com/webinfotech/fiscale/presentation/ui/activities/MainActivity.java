package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;

import com.webinfotech.fiscale.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_branch) void onBranchClicked() {
        Intent intent = new Intent(this, ServicePointLoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_employee_login) void onEmployeeLoginClicked() {
        Intent intent = new Intent(this, MemberLoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_me_login) void onExecutiveLoginClicked() {
        Intent intent = new Intent(this, ExecutiveLoginActivity.class);
        startActivity(intent);
    }

}
