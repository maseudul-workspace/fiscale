package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.ClientDetails;

public interface ClientDetailsPresenter {
    void fetchUserDetails(int userId);
    interface View {
        void loadClientDetails(ClientDetails clientDetails);
        void showLoader();
        void hideLoader();
    }
}
