package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.JobDescList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.ViewHolder> {

    public interface Callback {
        void onLayoutClicked(int jobId, String jobName);
    }

    Context mContext;
    JobDescList[] jobDescLists;
    Callback mCallback;

    public JobListAdapter(Context mContext, JobDescList[] jobDescLists, Callback callback) {
        this.mContext = mContext;
        this.jobDescLists = jobDescLists;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_jobs, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewJobName.setText(jobDescLists[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onLayoutClicked(jobDescLists[position].id, jobDescLists[position].name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobDescLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_job_name)
        TextView txtViewJobName;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
