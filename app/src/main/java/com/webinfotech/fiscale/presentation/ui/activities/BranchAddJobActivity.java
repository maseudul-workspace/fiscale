package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.presentation.presenters.BranchAddJobPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.BranchAddJobPresenterImpl;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.ArrayList;

public class BranchAddJobActivity extends BranchBaseActivity implements BranchAddJobPresenter.View {

    @BindView(R.id.layout_job_description)
    LinearLayout layoutJobDescription;
    @BindView(R.id.txt_view_job_description)
    TextView txtViewJobDescription;
    @BindView(R.id.txt_view_client_name)
    TextView txtViewClientName;
    @BindView(R.id.txt_view_fathers_name)
    TextView txtViewFathersName;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.txt_view_pan)
    TextView txtViewPan;
    @BindView(R.id.txt_view_constitution)
    TextView txtViewConstitution;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.layout_1)
    View layout1;
    @BindView(R.id.layout_2)
    View layout2;
    @BindView(R.id.edit_text_search_client)
    EditText editTextSearchClient;
    @BindView(R.id.view_job)
    View viewJob;
    BranchAddJobPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    JobDescList[] jobDescLists;
    ArrayList<Integer> jobIds = new ArrayList<>();
    @BindView(R.id.btn_submit)
    MaterialButton btnSubmit;
    int clientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_branch_add_job);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        mPresenter.fetchJobDescList();
    }

    public void initialisePresenter() {
        mPresenter = new BranchAddJobPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadClientDetails(ClientList clientList) {
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.VISIBLE);
        txtViewClientName.setText(clientList.name);
        txtViewConstitution.setText(clientList.constitution);
        txtViewDob.setText(clientList.dob);
        txtViewFathersName.setText(clientList.fatherName);
        txtViewGender.setText(clientList.gender);
        txtViewPan.setText(clientList.pan);
        layoutJobDescription.setVisibility(View.VISIBLE);
        txtViewJobDescription.setVisibility(View.VISIBLE);
        viewJob.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
        clientId = clientList.clientId;
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadJobDescList(JobDescList[] jobDescList) {
        this.jobDescLists = jobDescList;
        for (int i = 0; i < jobDescList.length; i++) {
            View customLayout = LayoutInflater.from(this).inflate(R.layout.layout_job_description, null, false);
            layoutJobDescription.addView(customLayout);
            CheckBox checkBox = (CheckBox) customLayout.findViewById(R.id.job_checkbox);
            checkBox.setText(jobDescList[i].name);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        for (int j = 0; j < jobDescList.length; j++) {
                            if (checkBox.getText().toString().equals(jobDescList[j].name)) {
                                jobIds.add(jobDescList[j].id);
                                break;
                            }
                        }
                    } else {
                        for (int j = 0; j < jobDescList.length; j++) {
                            if (checkBox.getText().toString().equals(jobDescList[j].name)) {
                                removeJobIds(jobDescList[j].id);
                                break;
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onLoadClientDetailsFail() {
        layoutJobDescription.setVisibility(View.GONE);
        txtViewJobDescription.setVisibility(View.GONE);
        viewJob.setVisibility(View.GONE);
        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.GONE);
        btnSubmit.setVisibility(View.GONE);
    }

    private void removeJobIds(int id) {
        for (int i = 0; i < jobIds.size(); i++) {
            if (jobIds.get(i) == id) {
                jobIds.remove(i);
                break;
            }
        }
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @OnClick(R.id.btn_search) void onBtnSearchClicked() {
        if (editTextSearchClient.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Enter Mobile No/PAN", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.fetchClientDetails(editTextSearchClient.getText().toString());
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (jobIds.size() == 0) {
            Toasty.warning(this, "Please Enter Job", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.addClientExistingJobs(jobIds, clientId);
        }
    }

}
