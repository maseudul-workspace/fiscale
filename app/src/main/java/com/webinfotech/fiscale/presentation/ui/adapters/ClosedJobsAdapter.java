package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.Testing.ClosedJobs;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ClosedJobsAdapter extends RecyclerView.Adapter<ClosedJobsAdapter.ViewHolder> {

    Context mContext;
    ArrayList<ClosedJobs> closedJobs;

    public ClosedJobsAdapter(Context mContext, ArrayList<ClosedJobs> closedJobs) {
        this.mContext = mContext;
        this.closedJobs = closedJobs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_closed_jobs, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDate.setText(closedJobs.get(position).date);
        holder.txtViewClientName.setText(closedJobs.get(position).clientName);
        holder.txtViewClosedDate.setText(closedJobs.get(position).closedDate);
        holder.txtViewJobDesc.setText(closedJobs.get(position).jobDesc);
        holder.txtViewStatus.setText(closedJobs.get(position).status);
    }

    @Override
    public int getItemCount() {
        return closedJobs.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_job_desc)
        TextView txtViewJobDesc;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_client_name)
        TextView txtViewClientName;
        @BindView(R.id.txt_view_closed_date)
        TextView txtViewClosedDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
