package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.JobList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SpClientSearchJobListAdapter extends RecyclerView.Adapter<SpClientSearchJobListAdapter.ViewHolder> {

    public interface Callback {
        void onViewJobClicked(String jobId, int id);
    }

    JobList[] jobLists;
    Context mContext;
    Callback mCallback;

    public SpClientSearchJobListAdapter(Context mContext, JobList[] jobLists, Callback callback) {
        this.jobLists = jobLists;
        this.mContext = mContext;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_sp_client_search, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDate.setText(jobLists[position].createdAt);
        holder.txtViewJobDesc.setText(jobLists[position].description);
        switch (jobLists[position].status) {
            case 1:
                holder.txtViewStatus.setText("Processing");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_yellow_800));
                break;
            case 2:
                holder.txtViewStatus.setText("Working");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_yellow_800));
                break;
            case 3:
                holder.txtViewStatus.setText("Document Problem");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                break;
            case 4:
                holder.txtViewStatus.setText("Completed");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
                break;
        }
        holder.btnViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewJobClicked(jobLists[position].jobId, jobLists[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_job_desc)
        TextView txtViewJobDesc;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.btn_view_details)
        Button btnViewDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
