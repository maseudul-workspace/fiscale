package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.CloseJobs;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberCloseJobsAdapter extends RecyclerView.Adapter<MemberCloseJobsAdapter.ViewHolder> {

    Context mContext;
    CloseJobs[] closeJobs;

    public MemberCloseJobsAdapter(Context mContext, CloseJobs[] closeJobs) {
        this.mContext = mContext;
        this.closeJobs = closeJobs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_member_close_jobs, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewAssignedDate.setText(closeJobs[position].assignedDate);
        holder.txtViewClientName.setText(closeJobs[position].clientName);
        holder.txtViewClientPan.setText(closeJobs[position].pan);
        holder.txtViewJobDesc.setText(closeJobs[position].description);
        switch (closeJobs[position].status) {
            case 1:
                holder.txtViewStatus.setText("Processing");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_yellow_800));
                break;
            case 2:
                holder.txtViewStatus.setText("Working");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_yellow_800));
                break;
            case 3:
                holder.txtViewStatus.setText("Document Problem");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                break;
            case 4:
                holder.txtViewStatus.setText("Completed");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
                break;
        }
        holder.txtViewSpName.setText(closeJobs[position].branch_name);
        holder.txtViewClosedDate.setText(closeJobs[position].completedDate);
    }

    @Override
    public int getItemCount() {
        return closeJobs.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_assigned_date)
        TextView txtViewAssignedDate;
        @BindView(R.id.txt_view_client_name)
        TextView txtViewClientName;
        @BindView(R.id.txt_view_client_pan)
        TextView txtViewClientPan;
        @BindView(R.id.txt_view_job_desc)
        TextView txtViewJobDesc;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_sp_name)
        TextView txtViewSpName;
        @BindView(R.id.txt_view_closed_date)
        TextView txtViewClosedDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(CloseJobs[] closeJobs) {
        this.closeJobs = closeJobs;
        notifyDataSetChanged();
    }

}
