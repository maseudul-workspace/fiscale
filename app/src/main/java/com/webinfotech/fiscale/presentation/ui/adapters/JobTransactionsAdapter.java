package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.TransactionJob;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class JobTransactionsAdapter extends RecyclerView.Adapter<JobTransactionsAdapter.ViewHolder> {

    Context mContext;
    TransactionJob[] transactionJobs;

    public JobTransactionsAdapter(Context mContext, TransactionJob[] transactionJobs) {
        this.mContext = mContext;
        this.transactionJobs = transactionJobs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_job_transactions, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewAmount.setText("₹ " + transactionJobs[position].amount);
        holder.txtViewDate.setText(transactionJobs[position].createdAt);
        holder.txtViewJobId.setText(transactionJobs[position].jobId);
        holder.txtViewJobName.setText(transactionJobs[position].jobName);
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        if (transactionJobs[position].status == 1) {
            holder.txtViewStatus.setText("Waiting");
            holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_deep_orange_300));
        } else {
            holder.txtViewStatus.setText("Credited");
            holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
        }
    }

    @Override
    public int getItemCount() {
        return transactionJobs.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_job_id)
        TextView txtViewJobId;
        @BindView(R.id.txt_view_job_name)
        TextView txtViewJobName;
        @BindView(R.id.txt_view_amount)
        TextView txtViewAmount;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(TransactionJob[] transactionJobs) {
        this.transactionJobs = transactionJobs;
    }

}
