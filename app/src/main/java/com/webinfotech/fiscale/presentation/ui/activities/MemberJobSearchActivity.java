package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.Testing.JobSearch;
import com.webinfotech.fiscale.presentation.presenters.MemberJobDetailsPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.MemberJobDetailsPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.BranchJobSearchAdapter;
import com.webinfotech.fiscale.presentation.ui.adapters.JobRemarksAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.ArrayList;

public class MemberJobSearchActivity extends EmployeeBaseActivity implements MemberJobDetailsPresenter.View {

    @BindView(R.id.txt_view_job_id)
    TextView txtViewJobId;
    @BindView(R.id.txt_view_client_name)
    TextView txtViewClientName;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.txt_view_pan)
    TextView txtViewPan;
    @BindView(R.id.txt_view_mobile)
    TextView txtViewMobile;
    @BindView(R.id.txt_view_constitution)
    TextView txtViewConstitution;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.txt_view_job_desc)
    TextView txtViewJobDesc;
    @BindView(R.id.txt_view_status)
    TextView txtViewStatus;
    @BindView(R.id.recycler_view_remarks)
    RecyclerView recyclerViewRemarks;
    @BindView(R.id.layout_1)
    View layout1;
    @BindView(R.id.layout_2)
    View layout2;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;
    @BindView(R.id.edit_text_search_job)
    EditText editTextSearchJob;
    @BindView(R.id.main_layout)
    View mainLayout;
    ProgressDialog progressDialog;
    MemberJobDetailsPresenterImpl mPresenter;
    int id;
    int clientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_member_job_search);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Job Search");
        setProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new MemberJobDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadMemberJobDetails(JobSearchDetailsData jobSearchDetailsData, JobRemarksAdapter adapter) {
        mainLayout.setVisibility(View.VISIBLE);
        txtViewClientName.setText(jobSearchDetailsData.clientName);
        txtViewConstitution.setText(jobSearchDetailsData.constitution);
        txtViewDob.setText(jobSearchDetailsData.dob);
        txtViewGender.setText(jobSearchDetailsData.gender);
        txtViewJobId.setText(jobSearchDetailsData.jobId);
        txtViewMobile.setText(jobSearchDetailsData.clientMobile);
        txtViewPan.setText(jobSearchDetailsData.clientPan);
        txtViewJobDesc.setText(jobSearchDetailsData.jobDesc);
        switch (jobSearchDetailsData.status) {
            case 1:
                txtViewStatus.setText("Processing");
                txtViewStatus.setTextColor(getResources().getColor(R.color.md_yellow_800));
                break;
            case 2:
                txtViewStatus.setText("Working");
                txtViewStatus.setTextColor(getResources().getColor(R.color.md_yellow_800));
                break;
            case 3:
                txtViewStatus.setText("Document Problem");
                txtViewStatus.setTextColor(getResources().getColor(R.color.red2));
                break;
            case 4:
                txtViewStatus.setText("Completed");
                txtViewStatus.setTextColor(getResources().getColor(R.color.green2));
                break;

        }

        recyclerViewRemarks.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewRemarks.setAdapter(adapter);
        id = jobSearchDetailsData.id;
        clientId = jobSearchDetailsData.clientId;
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (editTextSearchJob.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Enter Job No", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.fetchMemberJobDetails(editTextSearchJob.getText().toString());
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_add_remarks) void addRemarks() {
        if (editTextRemarks.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Enter Remarks", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.addRemarks(editTextRemarks.getText().toString(), id);
        }
    }

    @OnClick(R.id.btn_view_client) void onViewClientClicked() {
        Intent intent = new Intent(this, ViewClientDetailsActivity.class);
        intent.putExtra("clientId", clientId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_edit_job) void onEditJobClicked() {
        Intent intent = new Intent(this, MemberJobEditActivity.class);
        intent.putExtra("jobId", id);
        startActivity(intent);
    }

}
