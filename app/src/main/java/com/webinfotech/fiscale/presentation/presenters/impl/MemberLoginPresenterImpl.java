package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.CheckMemberLoginInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.CheckMemberLoginInteractorImpl;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.MemberLoginPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberLoginPresenterImpl extends AbstractPresenter implements MemberLoginPresenter, CheckMemberLoginInteractor.Callback {

    Context mContext;
    MemberLoginPresenter.View mView;
    CheckMemberLoginInteractorImpl checkMemberLoginInteractor;

    public MemberLoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkMemberLogin(String email, String password) {
        checkMemberLoginInteractor = new CheckMemberLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkMemberLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.goToMemberPanel();
    }

    @Override
    public void onLoginFailed(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
