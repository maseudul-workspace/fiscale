package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.presentation.ui.adapters.JobRemarksAdapter;

public interface SpClientJobDetailsPresenter {
    void fetchClientJobDetails(String jobId);
    void addRemarks(String remarks, int jobId);
    interface View {
        void loadClientJobDetails(JobSearchDetailsData jobSearchDetailsData, JobRemarksAdapter adapter);
        void onAddRemarksSuccess();
        void onGettingDataFailed();
        void showLoader();
        void hideLoader();
    }
}
