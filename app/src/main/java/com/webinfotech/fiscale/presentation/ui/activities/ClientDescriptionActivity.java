package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.presentation.presenters.ClientDetailsPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.ClientDetailsPresenterImpl;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class ClientDescriptionActivity extends AppCompatActivity implements ClientDetailsPresenter.View {

    @BindView(R.id.txt_view_client_name)
    TextView txtViewClientName;
    @BindView(R.id.txt_view_dob)
    TextView txtViewClientDob;
    @BindView(R.id.txt_view_constitution)
    TextView txtViewConstitution;
    @BindView(R.id.txt_view_mobile)
    TextView txtViewClientMobile;
    @BindView(R.id.txt_view_fathers_name)
    TextView txtViewFathersName;
    @BindView(R.id.txt_view_pan)
    TextView txtViewPan;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.txt_view_email)
    TextView txtViewEmail;
    @BindView(R.id.txt_view_flat_no)
    TextView txtViewFlatNo;
    @BindView(R.id.txt_view_building_village)
    TextView txtViewBuildingVillage;
    @BindView(R.id.txt_view_po)
    TextView txtViewPo;
    @BindView(R.id.txt_view_ps)
    TextView txtViewPs;
    @BindView(R.id.txt_view_area)
    TextView txtViewArea;
    @BindView(R.id.txt_view_district)
    TextView txtViewDistrict;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.txt_view_pin)
    TextView txtViewPin;
    @BindView(R.id.txt_view_flat_no_business)
    TextView txtViewFlatNoBusiness;
    @BindView(R.id.txt_view_building_village_business)
    TextView txtViewBuildingVillageBusiness;
    @BindView(R.id.txt_view_po_business)
    TextView txtViewPoBusiness;
    @BindView(R.id.txt_view_ps_business)
    TextView txtViewPsBusiness;
    @BindView(R.id.txt_view_area_business)
    TextView txtViewAreaBusiness;
    @BindView(R.id.txt_view_district_business)
    TextView txtViewDistrictBusiness;
    @BindView(R.id.txt_view_state_business)
    TextView txtViewStateBusiness;
    @BindView(R.id.txt_view_pin_business)
    TextView txtViewPinBusiness;
    @BindView(R.id.txt_view_pin_trade_name)
    TextView txtViewTradeName;
    ClientDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_description);
        userId = getIntent().getIntExtra("userId", 0);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchUserDetails(userId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ClientDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadClientDetails(ClientDetails clientDetails) {
        txtViewClientName.setText(clientDetails.name);
        txtViewEmail.setText(clientDetails.email);
        txtViewClientMobile.setText(clientDetails.mobile);
        txtViewPan.setText(clientDetails.pan);
        txtViewFathersName.setText(clientDetails.fatherName);
        txtViewClientDob.setText(clientDetails.dob);
        txtViewGender.setText(clientDetails.gender);
        txtViewConstitution.setText(clientDetails.constitution);
        txtViewTradeName.setText(clientDetails.tradeName);
        txtViewFlatNo.setText(clientDetails.residentialAddress.flatNo);
        txtViewBuildingVillage.setText(clientDetails.residentialAddress.village);
        txtViewPo.setText(clientDetails.residentialAddress.po);
        txtViewPs.setText(clientDetails.residentialAddress.ps);
        txtViewArea.setText(clientDetails.residentialAddress.area);
        txtViewDistrict.setText(clientDetails.residentialAddress.dist);
        txtViewState.setText(clientDetails.residentialAddress.state);
        txtViewPin.setText(clientDetails.residentialAddress.pin);
        txtViewFlatNoBusiness.setText(clientDetails.businessAddress.flatNo);
        txtViewBuildingVillageBusiness.setText(clientDetails.businessAddress.village);
        txtViewPoBusiness.setText(clientDetails.businessAddress.po);
        txtViewPsBusiness.setText(clientDetails.businessAddress.ps);
        txtViewAreaBusiness.setText(clientDetails.businessAddress.area);
        txtViewDistrictBusiness.setText(clientDetails.businessAddress.dist);
        txtViewStateBusiness.setText(clientDetails.businessAddress.state);
        txtViewPinBusiness.setText(clientDetails.businessAddress.pin);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
