package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.UserInfo;

public class EmployeeBaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    Drawer drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_base);
        getUserData();
    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setDrawer();

    }

    public void setDrawer() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
//        actionbar.setDisplayShowTitleEnabled(false);

// Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .addProfiles(new ProfileDrawerItem().withName("Fiscale").withIcon(R.drawable.logo))
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("OPEN JOBS").withSelectable(false).withIdentifier(1),
                        new PrimaryDrawerItem().withName("CLOSED JOBS").withSelectable(false).withIdentifier(2),
                        new PrimaryDrawerItem().withName("SEARCH CLIENT").withSelectable(false).withIdentifier(3),
                        new PrimaryDrawerItem().withName("SEARCH JOBS").withSelectable(false).withIdentifier(4),
                        new ExpandableDrawerItem().withName("TRANSACTION").withSelectable(false)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("JOB").withLevel(2).withIdentifier(5).withSelectable(false),
                                        new SecondaryDrawerItem().withName("WALLET").withLevel(2).withIdentifier(6).withSelectable(false)
                                ),
                        new PrimaryDrawerItem().withName("LOG OUT").withSelectable(false).withIdentifier(7)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            switch (((int) drawerItem.getIdentifier())) {
                                case 1:
                                    break;
                                case 2:
                                    Intent memberCloseJobIntent = new Intent(getApplicationContext(), MemberCloseJobsActivity.class);
                                    startActivity(memberCloseJobIntent);
                                    break;
                                case 3:
                                    Intent employeeClientSearchIntent = new Intent(getApplicationContext(), MemberClientSearchActivity.class);
                                    startActivity(employeeClientSearchIntent);
                                    break;
                                case 4:
                                    Intent employeeJobSearchActivity = new Intent(getApplicationContext(), MemberJobSearchActivity.class);
                                    startActivity(employeeJobSearchActivity);
                                    break;
                                case 5:
                                    Intent jobTransactionActivity = new Intent(getApplicationContext(), MemberJobTransactionsActivity.class);
                                    startActivity(jobTransactionActivity);
                                    break;
                                case 6:
                                    Intent walletIntent = new Intent(getApplicationContext(), MemberWalletHistoryActivity.class);
                                    startActivity(walletIntent);
                                    break;
                                case 7:
                                    Intent logoutIntent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(logoutIntent);
                                    break;
                            }
                        }
                        return false;
                    }
                }).build();
    }

    private void getUserData() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo !=  null) {
            Log.e("LogMsg", "Api Token: " + userInfo.api_token);
            Log.e("LogMsg", "User Id: " + userInfo.id);
        }
    }

}
