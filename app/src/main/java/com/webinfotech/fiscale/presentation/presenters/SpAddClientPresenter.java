package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.JobDescList;

import java.util.ArrayList;

public interface SpAddClientPresenter {
    void fetchJobDescList();
    void registerClient(
            String name,
            String email,
            String mobile,
            String pan,
            String fatherName,
            String dob,
            String gender,
            String constitution,
            String tradeName,
            String flatNo,
            String village,
            String po,
            String ps,
            String area,
            String district,
            String state,
            String pin,
            String flatNoBusiness,
            String villageBusiness,
            String poBusiness,
            String psBusiness,
            String areaBusiness,
            String districtBusiness,
            String stateBusiness,
            String pinBusiness,
            ArrayList<Integer> jobIds
    );
    interface View {
        void loadJobDescList(JobDescList[] jobDescLists);
        void onRegisterSuccess();
        void showLoader();
        void hideLoader();
    }
}
