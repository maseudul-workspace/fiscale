package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchJobTransactionsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchJobTransactionsInteractorImpl;
import com.webinfotech.fiscale.domain.models.TransactionJob;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.JobTransactionsPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.JobTransactionsAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class JobTransactionPresenterImpl extends AbstractPresenter implements JobTransactionsPresenter, FetchJobTransactionsInteractor.Callback {

    Context mContext;
    JobTransactionsPresenter.View mView;
    FetchJobTransactionsInteractorImpl fetchJobTransactionsInteractor;
    TransactionJob[] newTransactionJobs;
    JobTransactionsAdapter adapter;

    public JobTransactionPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchJobTransactions(String startDate, String endDate, int page, String type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            if (type.equals("refresh")) {
                newTransactionJobs = null;
            }
            fetchJobTransactionsInteractor = new FetchJobTransactionsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, startDate, endDate, userInfo.id, page);
            fetchJobTransactionsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingJobTransactionsSuccess(TransactionJob[] transactionJobs, int totalPage) {
        if (transactionJobs == null) {
            mView.onDataFetchFailed();
            Toasty.error(mContext, "No Data Found", Toast.LENGTH_SHORT).show();
        } else {
            TransactionJob[] tempTransactionJobs;
            tempTransactionJobs = newTransactionJobs;
            try {
                int len1 = tempTransactionJobs.length;
                int len2 = transactionJobs.length;
                newTransactionJobs = new TransactionJob[len1 + len2];
                System.arraycopy(tempTransactionJobs, 0, newTransactionJobs, 0, len1);
                System.arraycopy(transactionJobs, 0, newTransactionJobs, len1, len2);
                adapter.updateData(newTransactionJobs);
                adapter.notifyDataSetChanged();
            }catch (NullPointerException e){
                newTransactionJobs = transactionJobs;
                adapter = new JobTransactionsAdapter(mContext, transactionJobs);
                mView.loadAdapter(adapter, totalPage);
            }
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingJobTransactionsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onDataFetchFailed();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
