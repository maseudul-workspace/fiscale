package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.presentation.ui.adapters.MemberOpenJobsAdapter;
import com.webinfotech.fiscale.presentation.ui.adapters.OpenJobsAdapter;

public interface MemberOpenJobPresenter {
    void fetchOpenJobs(int pageNo, String type);
    interface View {
        void loadData(MemberOpenJobsAdapter openJobsAdapter, int totalPage);
        void goToJobDetails(String jobId, int id);
        void showLoader();
        void hideLoader();
    }
}
