package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.Button;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.presentation.presenters.ExecutiveWalletHistoryPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.ExecutiveWalletHistoryPresenterImpl;
import com.webinfotech.fiscale.presentation.presenters.impl.MemberWalletHistoryPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class ExecutiveWalletHistoryActivity extends ExecutiveBaseActivity implements ExecutiveWalletHistoryPresenter.View {

    @BindView(R.id.recycler_view_wallet_history)
    RecyclerView recyclerViewWalletHistory;
    @BindView(R.id.btn_wallet_info)
    Button btnWalletInfo;
    ProgressDialog progressDialog;
    ExecutiveWalletHistoryPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_executive_wallet_history);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchWalletHistory(pageNo, "refresh");
    }

    private void initialisePresenter() {
        mPresenter = new ExecutiveWalletHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
    }

    @Override
    public void loadData(WalletHistoryAdapter walletHistoryAdapter, String amount, int totalPage) {
        btnWalletInfo.setText("Wallet Balance: " + amount);
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewWalletHistory.setAdapter(walletHistoryAdapter);
        recyclerViewWalletHistory.setLayoutManager(layoutManager);
        recyclerViewWalletHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchWalletHistory(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void onGettingDataFailed() {

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

}
