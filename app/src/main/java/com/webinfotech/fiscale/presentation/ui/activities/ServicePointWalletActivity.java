package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.WalletHistoryPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class ServicePointWalletActivity extends AppCompatActivity implements WalletHistoryPresenter.View {

    @BindView(R.id.recycler_view_wallet_history)
    RecyclerView recyclerViewWalletHistory;
    @BindView(R.id.btn_wallet_info)
    Button btnWalletInfo;
    WalletHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_point_wallet);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchWalletHistory();
    }

    private void initialisePresenter() {
        mPresenter = new WalletHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(WalletHistoryAdapter adapter, String amount) {
        btnWalletInfo.setText("Wallet Balance: " + amount);
        recyclerViewWalletHistory.setAdapter(adapter);
        recyclerViewWalletHistory.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
