package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.Testing.JobSearch;
import com.webinfotech.fiscale.presentation.ui.adapters.BranchJobSearchAdapter;

import java.util.ArrayList;

public class BranchJobSearchActivity extends BranchBaseActivity {

    @BindView(R.id.recycler_view_branch_job_search)
    RecyclerView recyclerViewBranchJobSearch;
    ArrayList<JobSearch> jobSearchArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_branch_job_search);
        ButterKnife.bind(this);
        setRecyclerViewBranchJobSearch();
    }

    public void setRecyclerViewBranchJobSearch() {
        jobSearchArrayList = new ArrayList<>();
        JobSearch jobSearch1 = new JobSearch("2020-01-24 18:21:13", "Employee", "Good Job");
        JobSearch jobSearch2 = new JobSearch("2020-01-24 18:21:16", "Employee", "Bad Job");
        JobSearch jobSearch3 = new JobSearch("2020-01-24 18:22:09", "Employee", "Hillarious");
        JobSearch jobSearch4 = new JobSearch("2020-01-24 18:21:13", "Employee", "Good Job");
        jobSearchArrayList.add(jobSearch1);
        jobSearchArrayList.add(jobSearch2);
        jobSearchArrayList.add(jobSearch3);
        jobSearchArrayList.add(jobSearch4);
        BranchJobSearchAdapter branchJobSearchAdapter = new BranchJobSearchAdapter(this, jobSearchArrayList);
        recyclerViewBranchJobSearch.setAdapter(branchJobSearchAdapter);
        recyclerViewBranchJobSearch.setLayoutManager(new LinearLayoutManager(this));
    }


}
