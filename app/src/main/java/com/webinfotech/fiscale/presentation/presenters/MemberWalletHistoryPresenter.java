package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.presentation.ui.adapters.WalletHistoryAdapter;

public interface MemberWalletHistoryPresenter {
    void fetchMemberWalletHistory(int page, String type);
    interface View {
        void loadData(WalletHistoryAdapter walletHistoryAdapter, String amount, int totalPage);
        void onGettingDataFailed();
        void showLoader();
        void hideLoader();
    }
}
