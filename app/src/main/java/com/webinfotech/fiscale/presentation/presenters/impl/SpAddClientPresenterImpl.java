package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetJobDescListInteractor;
import com.webinfotech.fiscale.domain.interactors.RegisterClientInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.GetJobDescListInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.RegisterClientInteractorImpl;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.SpAddClientPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class SpAddClientPresenterImpl extends AbstractPresenter implements SpAddClientPresenter, GetJobDescListInteractor.Callback, RegisterClientInteractor.Callback {

    Context mContext;
    SpAddClientPresenter.View mView;
    GetJobDescListInteractorImpl getJobDescListInteractor;
    RegisterClientInteractorImpl registerClientInteractor;
    AndroidApplication androidApplication;

    public SpAddClientPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchJobDescList() {
        getJobDescListInteractor = new GetJobDescListInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        getJobDescListInteractor.execute();
    }

    @Override
    public void registerClient(String name, String email, String mobile, String pan, String fatherName, String dob, String gender, String constitution, String tradeName, String flatNo, String village, String po, String ps, String area, String district, String state, String pin, String flatNoBusiness, String villageBusiness, String poBusiness, String psBusiness, String areaBusiness, String districtBusiness, String stateBusiness, String pinBusiness, ArrayList<Integer> jobIds) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            registerClientInteractor = new RegisterClientInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, name, email, mobile, pan, fatherName, dob, gender, constitution, tradeName, userInfo.id, flatNo, village, po, ps, area, district, state, pin, flatNoBusiness, villageBusiness, poBusiness, psBusiness, areaBusiness, districtBusiness, stateBusiness, pinBusiness, jobIds);
            registerClientInteractor.execute();
        }
    }

    @Override
    public void onGettingJobDescListSuccess(JobDescList[] jobDescLists) {
        mView.loadJobDescList(jobDescLists);
    }

    @Override
    public void onGettingJobDescListFail(String errorMsg) {

    }

    @Override
    public void onRegisterClientSuccess() {
        mView.hideLoader();
        mView.onRegisterSuccess();
        Toasty.success(mContext, "Registration Successfull", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRegisterClientFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
