package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.OpenJobs;
import com.webinfotech.fiscale.presentation.ui.adapters.OpenJobsAdapter;

public interface OpenJobsPresenter {
    void fetchOpenJobs(int pageNo, String type);
    interface View {
        void loadData(OpenJobsAdapter openJobsAdapter, int totalPage);
        void goToJobDetails(String jobId, int id);
        void showLoader();
        void hideLoader();
    }
}
