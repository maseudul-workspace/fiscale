package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.Testing.MainJobs;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CorrectionAdapter extends RecyclerView.Adapter<CorrectionAdapter.ViewHolder> {

    public interface Callback {
        void onViewClicked(int id);
    }

    Context mContext;
    ArrayList<MainJobs> mainJobs;
    Callback mCallback;

    public CorrectionAdapter(Context mContext, ArrayList<MainJobs> mainJobs, Callback mCallback) {
        this.mContext = mContext;
        this.mainJobs = mainJobs;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_correction_jobs, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDate.setText(mainJobs.get(position).date);
        holder.txtViewClientName.setText(mainJobs.get(position).clientName);
        holder.txtViewJobDesc.setText(mainJobs.get(position).jobDesc);
        holder.txtViewStatus.setText(mainJobs.get(position).status);
    }

    @Override
    public int getItemCount() {
        return mainJobs.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_job_desc)
        TextView txtViewJobDesc;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_client_name)
        TextView txtViewClientName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
