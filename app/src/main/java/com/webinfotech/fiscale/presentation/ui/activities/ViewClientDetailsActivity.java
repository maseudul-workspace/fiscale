package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.MemberClientDetails;
import com.webinfotech.fiscale.presentation.presenters.EditClientPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.EditClientPresenterImpl;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class ViewClientDetailsActivity extends AppCompatActivity implements EditClientPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_fathers_name)
    EditText editTextFathersName;
    @BindView(R.id.txt_input_pan_layout)
    TextInputLayout txtInputPanLayout;
    @BindView(R.id.edit_text_pan)
    EditText editTextPan;
    @BindView(R.id.txt_input_mobile_layout)
    TextInputLayout txtInputMobileLayout;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.edit_text_flat_no)
    EditText editTextFlatNo;
    @BindView(R.id.txt_input_building_layout)
    TextInputLayout txtInputBuildingLayout;
    @BindView(R.id.edit_text_building_no)
    EditText editTextBuildingNo;
    @BindView(R.id.txt_input_po_layout)
    TextInputLayout txtInputPOLayout;
    @BindView(R.id.edit_text_po)
    EditText editTextPO;
    @BindView(R.id.txt_input_ps_layout)
    TextInputLayout txtInputPSLayout;
    @BindView(R.id.edit_text_ps)
    EditText editTextPS;
    @BindView(R.id.edit_text_area)
    EditText editTextArea;
    @BindView(R.id.txt_input_district_layout)
    TextInputLayout txtInputDistrictLayout;
    @BindView(R.id.edit_text_district)
    EditText editTextDistrict;
    @BindView(R.id.txt_input_state_layout)
    TextInputLayout txtInputStateLayout;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.txt_input_pin_layout)
    TextInputLayout txtInputPinLayout;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_business_flat_no)
    EditText editTextBusinessFlatNo;
    @BindView(R.id.txt_input_business_building_layout)
    TextInputLayout txtInputBusinessBuildingLayout;
    @BindView(R.id.edit_text_business_building_no)
    EditText editTextBusinessBuildingNo;
    @BindView(R.id.txt_input_business_po_layout)
    TextInputLayout txtInputBusinessPOLayout;
    @BindView(R.id.edit_text_business_po)
    EditText editTextBusinessPO;
    @BindView(R.id.txt_input_business_ps_layout)
    TextInputLayout txtInputBusinessPSLayout;
    @BindView(R.id.edit_text_business_ps)
    EditText editTextBusinessPS;
    @BindView(R.id.edit_text_business_area)
    EditText editTextBusinessArea;
    @BindView(R.id.txt_input_business_district_layout)
    TextInputLayout txtInputBusinessDistrictLayout;
    @BindView(R.id.edit_text_business_district)
    EditText editTextBusinessDistrict;
    @BindView(R.id.txt_input_business_state_layout)
    TextInputLayout txtInputBusinesStateLayout;
    @BindView(R.id.edit_text_business_state)
    EditText editTextBusinessState;
    @BindView(R.id.txt_input_business_pin_layout)
    TextInputLayout txtInputBusinessPinLayout;
    @BindView(R.id.edit_text_business_pin)
    EditText editTextBusinessPin;
    @BindView(R.id.edit_text_trade)
    EditText editTextTrade;
    @BindView(R.id.edit_text_gender)
    EditText editTextGender;
    @BindView(R.id.edit_text_constitution)
    EditText editTextConstitution;
    ProgressDialog progressDialog;
    EditClientPresenterImpl mPresenter;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_client_details);
        userId = getIntent().getIntExtra("clientId", 0);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        setProgressDialog();
        mPresenter.getClientDetails(userId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new EditClientPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadClientDetails(MemberClientDetails clientDetails) {
        editTextArea.setText(clientDetails.residentialAddress.area);
        editTextBuildingNo.setText(clientDetails.residentialAddress.village);
        editTextBusinessArea.setText(clientDetails.businessAddress.area);
        editTextBusinessBuildingNo.setText(clientDetails.businessAddress.village);
        editTextBusinessDistrict.setText(clientDetails.businessAddress.dist);
        editTextBusinessFlatNo.setText(clientDetails.businessAddress.flatNo);
        editTextBusinessPin.setText(clientDetails.businessAddress.pin);
        editTextBusinessPO.setText(clientDetails.businessAddress.po);
        editTextBusinessPS.setText(clientDetails.businessAddress.ps);
        editTextBusinessState.setText(clientDetails.businessAddress.state);
        editTextDistrict.setText(clientDetails.residentialAddress.dist);
        editTextEmail.setText(clientDetails.email);
        editTextFathersName.setText(clientDetails.fatherName);
        editTextFlatNo.setText(clientDetails.residentialAddress.flatNo);
        editTextMobile.setText(clientDetails.mobile);
        editTextName.setText(clientDetails.name);
        editTextPan.setText(clientDetails.pan);
        editTextPin.setText(clientDetails.residentialAddress.pin);
        editTextPO.setText(clientDetails.residentialAddress.po);
        editTextPS.setText(clientDetails.residentialAddress.ps);
        editTextState.setText(clientDetails.residentialAddress.state);
        editTextTrade.setText(clientDetails.tradeName);
        txtViewDob.setText(clientDetails.dob);
        editTextGender.setText(clientDetails.gender);
        editTextConstitution.setText(clientDetails.constitution);
    }

    @Override
    public void onEditSuccess() {

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
