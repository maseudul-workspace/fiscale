package com.webinfotech.fiscale.presentation.presenters;

public interface ExecutiveLoginPresenter {
    void checkExecutiveLogin(String email, String password);
    interface View {
        void showLoader();
        void hideLoader();
        void goToJobTransactions();
    }

}
