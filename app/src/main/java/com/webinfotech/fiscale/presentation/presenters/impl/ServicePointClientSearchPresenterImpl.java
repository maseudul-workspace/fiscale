package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.SearchClientWithJobsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.SearchClientWithJobsInteractorImpl;
import com.webinfotech.fiscale.domain.models.ClientSearchData;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.ServicePointClientSearchPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.SpClientSearchJobListAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ServicePointClientSearchPresenterImpl extends AbstractPresenter implements ServicePointClientSearchPresenter, SearchClientWithJobsInteractor.Callback, SpClientSearchJobListAdapter.Callback {

    Context mContext;
    ServicePointClientSearchPresenter.View mView;
    SearchClientWithJobsInteractorImpl searchClientWithJobsInteractor;
    AndroidApplication androidApplication;
    SpClientSearchJobListAdapter adapter;

    public ServicePointClientSearchPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void searchClientWithJobs(String searchKey) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            searchClientWithJobsInteractor = new SearchClientWithJobsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, searchKey);
            searchClientWithJobsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onSearchClientWithJobsSuccess(ClientSearchData clientSearchData) {
        adapter = new SpClientSearchJobListAdapter(mContext, clientSearchData.jobLists, this);
        mView.loadClientData(clientSearchData, adapter);
        mView.hideLoader();
    }

    @Override
    public void onSearchClientWithJobsFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onViewJobClicked(String jobId, int id) {
        mView.goToJobDetailsActivity(jobId, id);
    }
}
