package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.CheckSPloginInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.CheckSPloginInteractorImpl;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.CheckSPloginPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class CheckSPloginPresenterImpl extends AbstractPresenter implements CheckSPloginPresenter, CheckSPloginInteractor.Callback {

    Context mContext;
    CheckSPloginPresenter.View mView;
    CheckSPloginInteractorImpl checkSPloginInteractor;
    AndroidApplication androidApplication;

    public CheckSPloginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkSPlogin(String email, String password) {
        checkSPloginInteractor = new CheckSPloginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkSPloginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideLoader();
        Toasty.success(mContext, "Login Successfully", Toast.LENGTH_SHORT).show();
        mView.onGoToBranchOpenJobs();
    }

    @Override
    public void onLoginFailed(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
