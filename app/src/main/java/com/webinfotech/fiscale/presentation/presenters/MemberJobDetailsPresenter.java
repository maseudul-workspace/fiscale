package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.presentation.ui.adapters.JobRemarksAdapter;

public interface MemberJobDetailsPresenter {
    void fetchMemberJobDetails(String jobId);
    void addRemarks(String remarks, int jobId);
    interface View {
        void loadMemberJobDetails(JobSearchDetailsData jobSearchDetailsData, JobRemarksAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
