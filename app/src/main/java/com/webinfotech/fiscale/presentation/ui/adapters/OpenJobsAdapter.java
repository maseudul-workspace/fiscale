package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.OpenJobs;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenJobsAdapter extends RecyclerView.Adapter<OpenJobsAdapter.ViewHolder> {

    public interface Callback {
        void onViewBtnClicked(String jobId, int id);
    }

    Context mContext;
    OpenJobs[] openJobs;
    Callback mCallback;

    public OpenJobsAdapter(Context mContext, OpenJobs[] openJobs, Callback mCallback) {
        this.mContext = mContext;
        this.openJobs = openJobs;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_open_jobs, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDate.setText(openJobs[position].createdAt);
        holder.txtViewClientName.setText(openJobs[position].clientName);
        holder.txtViewClientPan.setText(openJobs[position].pan);
        holder.txtViewJobDesc.setText(openJobs[position].description);
        switch (openJobs[position].status) {
            case 1:
                holder.txtViewStatus.setText("Processing");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_yellow_800));
                break;
            case 2:
                holder.txtViewStatus.setText("Working");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_yellow_800));
                break;
            case 3:
                holder.txtViewStatus.setText("Document Problem");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                break;
            case 4:
                holder.txtViewStatus.setText("Completed");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
                break;
        }
        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewBtnClicked(openJobs[position].jobId, openJobs[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return openJobs.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_client_name)
        TextView txtViewClientName;
        @BindView(R.id.txt_view_client_pan)
        TextView txtViewClientPan;
        @BindView(R.id.txt_view_job_desc)
        TextView txtViewJobDesc;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.btn_view)
        Button btnView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(OpenJobs[] openJobs) {
        this.openJobs = openJobs;
        notifyDataSetChanged();
    }

}
