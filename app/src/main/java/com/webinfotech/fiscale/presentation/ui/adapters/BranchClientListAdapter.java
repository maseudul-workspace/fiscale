package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.ClientList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BranchClientListAdapter extends RecyclerView.Adapter<BranchClientListAdapter.ViewHolder>{

    public interface Callback {
        void onViewClicked(String mobile);
        void onEditClicked(int userId);
    }

    Context mContext;
    ClientList[] clientLists;
    Callback mCallback;

    public BranchClientListAdapter(Context mContext, ClientList[] clientLists, Callback callback) {
        this.mContext = mContext;
        this.clientLists = clientLists;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_branch_user_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        holder.txtViewName.setText(clientLists[position].name);
        holder.txtViewMobile.setText(clientLists[position].mobile);
        holder.txtViewPan.setText(clientLists[position].pan);
        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(clientLists[position].mobile);
            }
        });
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(clientLists[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return clientLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_name)
        TextView txtViewName;
        @BindView(R.id.txt_view_pan)
        TextView txtViewPan;
        @BindView(R.id.txt_view_mobile)
        TextView txtViewMobile;
        @BindView(R.id.btn_view)
        Button btnView;
        @BindView(R.id.btn_edit)
        Button btnEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ClientList[] clientLists) {
        this.clientLists = clientLists;
        notifyDataSetChanged();
    }

}
