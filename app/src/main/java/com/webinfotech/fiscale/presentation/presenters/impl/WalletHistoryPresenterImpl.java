package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchSpWalletHistoryInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchSpWalletHistoryInteractorImpl;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.domain.models.WalletHistoryData;
import com.webinfotech.fiscale.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WalletHistoryPresenterImpl extends AbstractPresenter implements WalletHistoryPresenter, FetchSpWalletHistoryInteractor.Callback {

    Context mContext;
    WalletHistoryPresenter.View mView;
    FetchSpWalletHistoryInteractorImpl fetchSpWalletHistoryInteractor;

    public WalletHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWalletHistory() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            fetchSpWalletHistoryInteractor = new FetchSpWalletHistoryInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.id, userInfo.api_token);
            fetchSpWalletHistoryInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onWalletFetchSuccess(WalletHistoryData walletHistoryData) {
        WalletHistoryAdapter walletHistoryAdapter = new WalletHistoryAdapter(mContext, walletHistoryData.walletHistories);
        mView.loadData(walletHistoryAdapter, walletHistoryData.amount);
        mView.hideLoader();
    }

    @Override
    public void onWalletFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
