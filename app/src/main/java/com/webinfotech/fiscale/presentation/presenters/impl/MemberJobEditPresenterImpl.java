package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetJobDescListInteractor;
import com.webinfotech.fiscale.domain.interactors.GetJobEditDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.UpdateMemberJobInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.GetJobDescListInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.GetJobEditDetailsInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.UpdateMemberJobInteractorImpl;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.MemberJobEditPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.JobListAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberJobEditPresenterImpl extends AbstractPresenter implements    MemberJobEditPresenter,
                                                                                GetJobEditDetailsInteractor.Callback,
                                                                                GetJobDescListInteractor.Callback,
                                                                                JobListAdapter.Callback,
                                                                                UpdateMemberJobInteractor.Callback
{

    Context mContext;
    MemberJobEditPresenter.View mView;
    GetJobEditDetailsInteractorImpl getJobEditDetailsInteractor;
    GetJobDescListInteractorImpl getJobDescListInteractor;
    int jobId;
    UpdateMemberJobInteractorImpl updateMemberJobInteractor;

    public MemberJobEditPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getMemberJobEditDetails(int jobId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            getJobEditDetailsInteractor = new GetJobEditDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, jobId);
            getJobEditDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void getJobList() {
        getJobDescListInteractor = new GetJobDescListInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        getJobDescListInteractor.execute();
    }

    @Override
    public void updateJob() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            updateMemberJobInteractor = new UpdateMemberJobInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, userInfo.id, jobId);
            updateMemberJobInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingJobEditDetailsSuccess(JobSearchDetailsData jobSearchDetailsData) {
        this.jobId = jobSearchDetailsData.jobTypeId;
        mView.loadData(jobSearchDetailsData);
        mView.hideLoader();
    }

    @Override
    public void onGettingJobEditDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingJobDescListSuccess(JobDescList[] jobDescLists) {
        JobListAdapter jobListAdapter = new JobListAdapter(mContext, jobDescLists, this);
        mView.loadJobAdapter(jobListAdapter);
    }

    @Override
    public void onGettingJobDescListFail(String errorMsg) {

    }

    @Override
    public void onLayoutClicked(int jobId, String jobName) {
        mView.setJobName(jobName);
        this.jobId = jobId;
    }

    @Override
    public void onUpdateJobSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Job Updated Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateJobFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
