package com.webinfotech.fiscale.presentation.presenters;

public interface MemberLoginPresenter {
    void checkMemberLogin(String email, String password);
    interface View {
        void showLoader();
        void hideLoader();
        void goToMemberPanel();
    }
}
