package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.AddJobRemarksInteractor;
import com.webinfotech.fiscale.domain.interactors.FetchMemberJobDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.AddJobRemarksInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.FetchMemberJobDetailsInteractorImpl;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.MemberJobDetailsPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.JobRemarksAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberJobDetailsPresenterImpl extends AbstractPresenter implements MemberJobDetailsPresenter,
                                                                                FetchMemberJobDetailsInteractor.Callback,
                                                                                AddJobRemarksInteractor.Callback {

    Context mContext;
    MemberJobDetailsPresenter.View mView;
    FetchMemberJobDetailsInteractorImpl fetchMemberJobDetailsInteractor;
    AndroidApplication androidApplication;
    AddJobRemarksInteractorImpl addJobRemarksInteractor;
    String jobId;

    public MemberJobDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMemberJobDetails(String jobId) {
        this.jobId = jobId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            fetchMemberJobDetailsInteractor = new FetchMemberJobDetailsInteractorImpl(mExecutor, mMainThread, mContext, new AppRepositoryImpl(), this, userInfo.api_token, jobId);
            fetchMemberJobDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void addRemarks(String remarks, int jobId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            addJobRemarksInteractor = new AddJobRemarksInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, userInfo.id, jobId, remarks);
            addJobRemarksInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingMemberJobDetailsSuccess(JobSearchDetailsData jobSearchDetailsData) {
        JobRemarksAdapter adapter = new JobRemarksAdapter(mContext, jobSearchDetailsData.remarks);
        mView.loadMemberJobDetails(jobSearchDetailsData, adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingMemberJobDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJobRemarksAddSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Successfully Added", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJobRemarksAddFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
