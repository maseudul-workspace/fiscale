package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.RejectJobInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchMemberOpenJobsInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.FetchOpenJobsIntercatorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.RejectJobInteractorImpl;
import com.webinfotech.fiscale.domain.models.OpenJobs;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.FetchMemberOpenJobsInteractor;
import com.webinfotech.fiscale.presentation.presenters.MemberOpenJobPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.MemberOpenJobsAdapter;
import com.webinfotech.fiscale.presentation.ui.adapters.OpenJobsAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberOpenJobPresenterImpl extends AbstractPresenter implements    MemberOpenJobPresenter,
                                                                                FetchMemberOpenJobsInteractor.Callback,
                                                                                MemberOpenJobsAdapter.Callback,
                                                                                RejectJobInteractor.Callback {

    Context mContext;
    MemberOpenJobPresenter.View mView;
    FetchMemberOpenJobsInteractorImpl  fetchMemberOpenJobsInteractor;
    AndroidApplication androidApplication;
    OpenJobs[] newOpenJobs;
    MemberOpenJobsAdapter adapter;
    RejectJobInteractorImpl rejectJobInteractor;
    int position;

    public MemberOpenJobPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOpenJobs(int pageNo, String type) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            if (type.equals("refresh")) {
                newOpenJobs = null;
            }
            fetchMemberOpenJobsInteractor = new FetchMemberOpenJobsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.api_token, userInfo.id, pageNo);
            fetchMemberOpenJobsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingMemberOpenJobsSuccess(OpenJobs[] openJobs, int totalPage) {
        OpenJobs[] tempOpenJobs;
        tempOpenJobs = newOpenJobs;
        try {
            int len1 = tempOpenJobs.length;
            int len2 = openJobs.length;
            newOpenJobs = new OpenJobs[len1 + len2];
            System.arraycopy(tempOpenJobs, 0, newOpenJobs, 0, len1);
            System.arraycopy(openJobs, 0, newOpenJobs, len1, len2);
            adapter.updateDataSet(newOpenJobs);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newOpenJobs = openJobs;
            adapter = new MemberOpenJobsAdapter(mContext, openJobs, this);
            mView.loadData(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingMemberOpenJobsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewBtnClicked(String jobId, int id) {
        mView.goToJobDetails(jobId, id);
    }

    @Override
    public void onRejectClicked(int jobId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            rejectJobInteractor = new RejectJobInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, jobId);
            rejectJobInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onRejectJobSuccess() {
        Toasty.success(mContext, "Successfully rejected", Toast.LENGTH_SHORT).show();
        fetchOpenJobs(1, "refresh");
    }

    @Override
    public void onRejectJobFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
