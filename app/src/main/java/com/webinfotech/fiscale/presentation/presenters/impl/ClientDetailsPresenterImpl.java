package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchClientDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchClientDetailsInteractorImpl;
import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.ClientDetailsPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ClientDetailsPresenterImpl extends AbstractPresenter implements    ClientDetailsPresenter,
                                                                                FetchClientDetailsInteractor.Callback
                                                                                {

    Context mContext;
    ClientDetailsPresenter.View mView;
    FetchClientDetailsInteractorImpl fetchClientDetailsInteractor;
    AndroidApplication androidApplication;

    public ClientDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserDetails(int userId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchClientDetailsInteractor = new FetchClientDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userId, userInfo.api_token);
            fetchClientDetailsInteractor.execute();
        }
    }

    @Override
    public void onFetchingClientDetailsSuccess(ClientDetails clientDetails) {
        mView.loadClientDetails(clientDetails);
        mView.hideLoader();
    }

    @Override
    public void onFetchingClientDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
