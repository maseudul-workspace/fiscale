package com.webinfotech.fiscale.presentation.presenters;

public interface JobSearchPresenter {
    void searchJob(String jobId);
}
