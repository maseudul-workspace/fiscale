package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.presentation.presenters.SpClientJobDetailsPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.SpClientJobDetailsPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.JobRemarksAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class JobSearchActivity extends AppCompatActivity implements SpClientJobDetailsPresenter.View {

    @BindView(R.id.txt_view_job_id)
    TextView txtViewJobId;
    @BindView(R.id.txt_view_client_name)
    TextView txtViewClientName;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.txt_view_pan)
    TextView txtViewPan;
    @BindView(R.id.txt_view_mobile)
    TextView txtViewMobile;
    @BindView(R.id.txt_view_constitution)
    TextView txtViewConstitution;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.txt_view_job_desc)
    TextView txtViewJobDesc;
    @BindView(R.id.txt_view_status)
    TextView txtViewStatus;
    @BindView(R.id.recycler_view_reamrks)
    RecyclerView recyclerViewRemarks;
    @BindView(R.id.layout_1)
    View layout1;
    @BindView(R.id.layout_2)
    View layout2;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;
    ProgressDialog progressDialog;
    SpClientJobDetailsPresenterImpl mPresenter;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.edit_text_search_job)
    EditText editTextSearchJob;
    int jobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_search);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
    }

    public void initialisePresenter() {
        mPresenter = new SpClientJobDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadClientJobDetails(JobSearchDetailsData jobSearchDetailsData, JobRemarksAdapter adapter) {
        mainLayout.setVisibility(View.VISIBLE);
        txtViewClientName.setText(jobSearchDetailsData.clientName);
        txtViewConstitution.setText(jobSearchDetailsData.constitution);
        txtViewDob.setText(jobSearchDetailsData.dob);
        txtViewGender.setText(jobSearchDetailsData.gender);
        txtViewJobId.setText(jobSearchDetailsData.jobId);
        txtViewMobile.setText(jobSearchDetailsData.clientMobile);
        txtViewPan.setText(jobSearchDetailsData.clientPan);
        txtViewJobDesc.setText(jobSearchDetailsData.jobDesc);
        switch (jobSearchDetailsData.status) {
            case 1:
                txtViewStatus.setText("Processing");
                txtViewStatus.setTextColor(getResources().getColor(R.color.md_yellow_800));
                break;
            case 2:
                txtViewStatus.setText("Working");
                txtViewStatus.setTextColor(getResources().getColor(R.color.md_yellow_800));
                break;
            case 3:
                txtViewStatus.setText("Document Problem");
                txtViewStatus.setTextColor(getResources().getColor(R.color.red2));
                break;
            case 4:
                txtViewStatus.setText("Completed");
                txtViewStatus.setTextColor(getResources().getColor(R.color.green2));
                break;

        }

        recyclerViewRemarks.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewRemarks.setAdapter(adapter);
        jobId = jobSearchDetailsData.id;
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (editTextSearchJob.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Enter Job Id", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.fetchClientJobDetails(editTextSearchJob.getText().toString());
            showLoader();
        }
    }

    @Override
    public void onAddRemarksSuccess() {
        editTextRemarks.setText("");
    }

    @Override
    public void onGettingDataFailed() {
        mainLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_add_remarks) void addRemarks() {
        if (editTextRemarks.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Enter Remarks", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.addRemarks(editTextRemarks.getText().toString(), jobId);
        }
    }

}
