package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.presentation.ui.adapters.JobTransactionsAdapter;

public interface JobTransactionsPresenter {
    void fetchJobTransactions(String startDate, String endDate, int page, String type);
    interface View {
        void loadAdapter(JobTransactionsAdapter adapter, int totalPage);
        void onDataFetchFailed();
        void showLoader();
        void hideLoader();
    }
}
