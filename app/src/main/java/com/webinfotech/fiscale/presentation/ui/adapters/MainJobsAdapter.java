package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.Testing.MainJobs;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainJobsAdapter extends RecyclerView.Adapter<MainJobsAdapter.ViewHolder> {

    Context mContext;
    ArrayList<MainJobs> mainJobsArrayList;

    public MainJobsAdapter(Context mContext, ArrayList<MainJobs> mainJobsArrayList) {
        this.mContext = mContext;
        this.mainJobsArrayList = mainJobsArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_open_jobs, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDate.setText(mainJobsArrayList.get(position).date);
        holder.txtViewJobDesc.setText(mainJobsArrayList.get(position).jobDesc);
        holder.txtViewStatus.setText(mainJobsArrayList.get(position).status);
        holder.txtViewClientName.setText(mainJobsArrayList.get(position).clientName);
    }

    @Override
    public int getItemCount() {
        return mainJobsArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_job_desc)
        TextView txtViewJobDesc;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_client_name)
        TextView txtViewClientName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
