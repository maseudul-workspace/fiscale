package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.AddJobRemarksInteractor;
import com.webinfotech.fiscale.domain.interactors.SearchClientJobDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.AddJobRemarksInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.SearchClientJobDetailsInteractorImpl;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.SpClientJobDetailsPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.JobRemarksAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SpClientJobDetailsPresenterImpl extends AbstractPresenter implements   SpClientJobDetailsPresenter,
                                                                                    SearchClientJobDetailsInteractor.Callback,
                                                                                    AddJobRemarksInteractor.Callback
{

    Context mContext;
    SpClientJobDetailsPresenter.View mView;
    SearchClientJobDetailsInteractorImpl searchClientJobDetailsInteractor;
    AndroidApplication androidApplication;
    AddJobRemarksInteractorImpl addJobRemarksInteractor;
    String jobId;

    public SpClientJobDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchClientJobDetails(String jobId) {
        this.jobId = jobId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            searchClientJobDetailsInteractor = new SearchClientJobDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, jobId);
            searchClientJobDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void addRemarks(String remarks, int id) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            addJobRemarksInteractor = new AddJobRemarksInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, userInfo.id, id, remarks);
            addJobRemarksInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingClientJobDetailsSuccess(JobSearchDetailsData jobSearchDetailsData) {
        JobRemarksAdapter adapter = new JobRemarksAdapter(mContext, jobSearchDetailsData.remarks);
        mView.loadClientJobDetails(jobSearchDetailsData, adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingClientJobDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onGettingDataFailed();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJobRemarksAddSuccess() {
        fetchClientJobDetails(jobId);
        mView.onAddRemarksSuccess();
    }

    @Override
    public void onJobRemarksAddFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
