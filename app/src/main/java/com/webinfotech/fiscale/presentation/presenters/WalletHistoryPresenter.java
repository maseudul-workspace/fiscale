package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.presentation.ui.adapters.WalletHistoryAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory();
    interface View {
        void loadData(WalletHistoryAdapter adapter, String amount);
        void showLoader();
        void hideLoader();
    }
}
