package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetClientListInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.GetClientListInteractorImpl;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.BranchClientListPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.BranchClientListAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

public class BranchClientListPresenterImpl extends AbstractPresenter implements BranchClientListPresenter, GetClientListInteractor.Callback, BranchClientListAdapter.Callback {

    Context mContext;
    BranchClientListPresenter.View mView;
    GetClientListInteractorImpl getClientListInteractor;
    AndroidApplication androidApplication;
    ClientList[] newClientList = null;
    BranchClientListAdapter adapter;

    public BranchClientListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchClientList(int page, String type) {
        if (type.equals("refresh")) {
            newClientList = null;
        }
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getClientListInteractor = new GetClientListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, userInfo.id, page);
            getClientListInteractor.execute();
        }
    }

    @Override
    public void onGettingClientListSuccess(ClientList[] clientLists, int totalPage) {
        if(clientLists.length > 0){
            ClientList[] tempClientLists;
            tempClientLists = newClientList;
            try {
                int len1 = tempClientLists.length;
                int len2 = clientLists.length;
                newClientList = new ClientList[len1 + len2];
                System.arraycopy(tempClientLists, 0, newClientList, 0, len1);
                System.arraycopy(clientLists, 0, newClientList, len1, len2);
                adapter.updateDataSet(newClientList);
                adapter.notifyDataSetChanged();
            } catch (NullPointerException e) {
                newClientList = clientLists;
                adapter = new BranchClientListAdapter(mContext, clientLists, this);
                mView.loadAdapter(adapter, totalPage);
            }
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingClientListFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onViewClicked(String mobile) {
        mView.goToClientDetails(mobile);
    }

    @Override
    public void onEditClicked(int userId) {
        mView.goToClientDescription(userId);
    }
}
