package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.Remarks;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class JobRemarksAdapter extends RecyclerView.Adapter<JobRemarksAdapter.ViewHolder> {

    Context mContext;
    Remarks[] remarks;

    public JobRemarksAdapter(Context mContext, Remarks[] remarks) {
        this.mContext = mContext;
        this.remarks = remarks;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_remarks_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDate.setText(remarks[position].createdAt);
        holder.txtViewCommentedBy.setText(remarks[position].remarksByName);
        holder.txtViewRemarks.setText(remarks[position].remarks);
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
    }

    @Override
    public int getItemCount() {
        return remarks.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_commented_by)
        TextView txtViewCommentedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
