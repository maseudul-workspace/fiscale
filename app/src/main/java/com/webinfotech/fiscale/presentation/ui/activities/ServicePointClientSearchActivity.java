package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.ClientSearchData;
import com.webinfotech.fiscale.presentation.presenters.ServicePointClientSearchPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.ServicePointClientSearchPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.SpClientSearchJobListAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class ServicePointClientSearchActivity extends AppCompatActivity implements ServicePointClientSearchPresenter.View {

    @BindView(R.id.txt_view_client_name)
    TextView txtViewClientName;
    @BindView(R.id.txt_view_fathers_name)
    TextView txtViewFathersName;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.txt_view_pan)
    TextView txtViewPan;
    @BindView(R.id.txt_view_constitution)
    TextView txtViewConstitution;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.layout_1)
    View layout1;
    @BindView(R.id.layout_2)
    View layout2;
    @BindView(R.id.edit_text_search_client)
    EditText editTextSearchClient;
    @BindView(R.id.header_recycler_view)
    View headerRecyclerView;
    @BindView(R.id.recycler_view_branch_job_search)
    RecyclerView recyclerViewBranchJobSearch;
    ServicePointClientSearchPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp_client_search);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
    }

    public void initialisePresenter() {
        mPresenter = new ServicePointClientSearchPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadClientData(ClientSearchData clientSearchData, SpClientSearchJobListAdapter adapter) {
        txtViewClientName.setText(clientSearchData.name);
        txtViewConstitution.setText(clientSearchData.constitution);
        txtViewDob.setText(clientSearchData.dob);
        txtViewFathersName.setText(clientSearchData.fatherName);
        txtViewGender.setText(clientSearchData.gender);
        txtViewPan.setText(clientSearchData.pan);
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.VISIBLE);

        recyclerViewBranchJobSearch.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewBranchJobSearch.setAdapter(adapter);
        recyclerViewBranchJobSearch.setVisibility(View.VISIBLE);
        headerRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGettingDataFailed() {
        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.GONE);
        recyclerViewBranchJobSearch.setVisibility(View.GONE);
        headerRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void goToJobDetailsActivity(String jobId, int id) {
        Intent intent = new Intent(this, SpClientJobDetailsActivity.class);
        intent.putExtra("jobId", jobId);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (editTextSearchClient.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Enter Pan/Mobile No", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.searchClientWithJobs(editTextSearchClient.getText().toString());
        }
    }

}
