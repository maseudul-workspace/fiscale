package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.view.View;

import com.webinfotech.fiscale.R;

public class EmployeeClientSearchActivity extends EmployeeBaseActivity {

    @BindView(R.id.layout_personal_details)
    View layoutPersonalDetails;
    @BindView(R.id.layout_residential_address)
    View layoutResidentialAddress;
    @BindView(R.id.layout_business_address)
    View layoutBusinessAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_employee_client_search);
        getSupportActionBar().setTitle("Search Client");
        ButterKnife.bind(this);
    }

    @OnClick(R.id.layout_personal_details_arrow) void onLayoutPersonalDetailsArrow() {
        if (layoutPersonalDetails.getVisibility() == View.VISIBLE) {
            layoutPersonalDetails.setVisibility(View.GONE);
        } else {
            layoutPersonalDetails.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_residential_address_arrow) void onLayoutResidentialAddressClicked() {
        if (layoutResidentialAddress.getVisibility() == View.VISIBLE) {
            layoutResidentialAddress.setVisibility(View.GONE);
        } else {
            layoutResidentialAddress.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_business_address_arrow) void onLayoutBusinessAddressArrowClicked() {
        if (layoutBusinessAddress.getVisibility() == View.VISIBLE) {
            layoutBusinessAddress.setVisibility(View.GONE);
        } else {
            layoutBusinessAddress.setVisibility(View.VISIBLE);
        }
    }

}
