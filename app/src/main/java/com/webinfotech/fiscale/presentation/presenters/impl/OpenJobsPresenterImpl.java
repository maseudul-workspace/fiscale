package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchOpenJobsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchOpenJobsIntercatorImpl;
import com.webinfotech.fiscale.domain.models.OpenJobs;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.OpenJobsPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.OpenJobsAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OpenJobsPresenterImpl extends AbstractPresenter implements OpenJobsPresenter, FetchOpenJobsInteractor.Callback, OpenJobsAdapter.Callback {

    Context mContext;
    OpenJobsPresenter.View mView;
    FetchOpenJobsIntercatorImpl fetchOpenJobsIntercator;
    AndroidApplication androidApplication;
    OpenJobs[] newOpenJobs;
    OpenJobsAdapter adapter;

    public OpenJobsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOpenJobs(int pageNo, String type) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            if (type.equals("refresh")) {
                newOpenJobs = null;
            }
            fetchOpenJobsIntercator = new FetchOpenJobsIntercatorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.api_token, userInfo.id, pageNo);
            fetchOpenJobsIntercator.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingOpenJobsSuccess(OpenJobs[] openJobs, int totalPage) {
        OpenJobs[] tempOpenJobs;
        tempOpenJobs = newOpenJobs;
        try {
            int len1 = tempOpenJobs.length;
            int len2 = openJobs.length;
            newOpenJobs = new OpenJobs[len1 + len2];
            System.arraycopy(tempOpenJobs, 0, newOpenJobs, 0, len1);
            System.arraycopy(openJobs, 0, newOpenJobs, len1, len2);
            adapter.updateDataSet(newOpenJobs);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newOpenJobs = openJobs;
            adapter = new OpenJobsAdapter(mContext, openJobs, this);
            mView.loadData(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingOpenJobFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewBtnClicked(String jobId, int id) {
        mView.goToJobDetails(jobId, id);
    }
}
