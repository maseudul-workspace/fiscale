package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.presentation.presenters.ExecutiveLoginPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.ExecutiveLoginPresenterImpl;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class ExecutiveLoginActivity extends AppCompatActivity implements ExecutiveLoginPresenter.View {

    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout txtInputPasswordLayout;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    ProgressDialog progressDialog;
    ExecutiveLoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_executive_login);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ExecutiveLoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_log_in) void onLoginBtnClicked() {
        txtInputEmailLayout.setError("");
        txtInputPasswordLayout.setError("");
        if (editTextEmail.getText().toString().trim().isEmpty()) {
            txtInputEmailLayout.setError("Email Required");
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            txtInputPasswordLayout.setError("Password Required");
        } else {
            mPresenter.checkExecutiveLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToJobTransactions() {
        Intent intent = new Intent(this, ExecutiveJobTransactionActivity.class);
        startActivity(intent);
    }
}
