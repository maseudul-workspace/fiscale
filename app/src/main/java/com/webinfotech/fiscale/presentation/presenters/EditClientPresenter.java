package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.MemberClientDetails;

public interface EditClientPresenter {
    void getClientDetails(int id);
    void editClient(
            int id,
            int residentialAddressId,
            int businessAddressId,
            String name,
            String email,
            String mobile,
            String pan,
            String fatherName,
            String dob,
            String gender,
            String constitution,
            String tradeName,
            String flatNo,
            String village,
            String po,
            String ps,
            String area,
            String district,
            String state,
            String pin,
            String flatNoBusiness,
            String villageBusiness,
            String poBusiness,
            String psBusiness,
            String areaBusiness,
            String districtBusiness,
            String stateBusiness,
            String pinBusiness
    );
    interface View {
        void loadClientDetails(MemberClientDetails clientDetails);
        void onEditSuccess();
        void showLoader();
        void hideLoader();
    }
}
