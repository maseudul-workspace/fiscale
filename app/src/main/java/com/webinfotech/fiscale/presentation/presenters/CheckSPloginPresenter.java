package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.UserInfo;

public interface CheckSPloginPresenter {
    void checkSPlogin(String email, String password);
    interface View {
        void showLoader();
        void hideLoader();
        void onGoToBranchOpenJobs();
    }
}
