package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.TabHost;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.AddClientExistingJobInteractor;
import com.webinfotech.fiscale.domain.interactors.GetJobDescListInteractor;
import com.webinfotech.fiscale.domain.interactors.SearchClientInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.AddClientExistingJobInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.GetJobDescListInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.SearchClientInteractorImpl;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.BranchAddJobPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class BranchAddJobPresenterImpl extends AbstractPresenter implements BranchAddJobPresenter,
                                                                            SearchClientInteractor.Callback,
                                                                            GetJobDescListInteractor.Callback,
                                                                            AddClientExistingJobInteractor.Callback
{

    Context mContext;
    BranchAddJobPresenter.View mView;
    SearchClientInteractorImpl searchClientInteractor;
    AndroidApplication androidApplication;
    GetJobDescListInteractorImpl getJobDescListInteractor;
    AddClientExistingJobInteractorImpl addClientExistingJobInteractor;

    public BranchAddJobPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchClientDetails(String searchKey) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            mView.showLoader();
            searchClientInteractor = new SearchClientInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, searchKey);
            searchClientInteractor.execute();
        }
    }

    @Override
    public void fetchJobDescList() {
        getJobDescListInteractor = new GetJobDescListInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        getJobDescListInteractor.execute();
    }

    @Override
    public void addClientExistingJobs(ArrayList<Integer> jobIds, int clientId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            addClientExistingJobInteractor = new AddClientExistingJobInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, jobIds, clientId, userInfo.id);
            addClientExistingJobInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onSearchClientSuccess(ClientList clientList) {
        mView.loadClientDetails(clientList);
        mView.hideLoader();
    }

    @Override
    public void onSearchClientFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onLoadClientDetailsFail();
    }

    @Override
    public void onGettingJobDescListSuccess(JobDescList[] jobDescLists) {
        mView.loadJobDescList(jobDescLists);
    }

    @Override
    public void onGettingJobDescListFail(String errorMsg) {
    }

    @Override
    public void onAddExistingJobSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Job Added Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddExistinJobFail(String errorMsg, int loginError) {

    }
}
