package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchMemberWalletHistoryInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchMemberWalletHistoryInteractorImpl;
import com.webinfotech.fiscale.domain.models.OpenJobs;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.domain.models.WalletHistory;
import com.webinfotech.fiscale.domain.models.WalletHistoryData;
import com.webinfotech.fiscale.presentation.presenters.MemberWalletHistoryPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.MemberOpenJobsAdapter;
import com.webinfotech.fiscale.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberWalletHistoryPresenterImpl extends AbstractPresenter implements MemberWalletHistoryPresenter, FetchMemberWalletHistoryInteractor.Callback {

    Context mContext;
    MemberWalletHistoryPresenter.View mView;
    WalletHistory[] newWalletHistories;
    FetchMemberWalletHistoryInteractorImpl fetchMemberWalletHistoryInteractor;
    WalletHistoryAdapter adapter;

    public MemberWalletHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMemberWalletHistory(int page, String type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            if (type.equals("refresh")) {
                fetchMemberWalletHistoryInteractor = new FetchMemberWalletHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, userInfo.id, page);
                fetchMemberWalletHistoryInteractor.execute();
                mView.showLoader();
            }
        }
    }

    @Override
    public void onGettingWalletHistorySuccess(WalletHistoryData walletHistoryData, int totalPage) {
        if (walletHistoryData != null) {
            WalletHistory[] tempWalletHistories;
            tempWalletHistories = newWalletHistories;
            try {
                int len1 = tempWalletHistories.length;
                int len2 = walletHistoryData.walletHistories.length;
                newWalletHistories = new WalletHistory[len1 + len2];
                System.arraycopy(tempWalletHistories, 0, newWalletHistories, 0, len1);
                System.arraycopy(walletHistoryData.walletHistories, 0, newWalletHistories, len1, len2);
                adapter.updateData(newWalletHistories);
                adapter.notifyDataSetChanged();
            }catch (NullPointerException e){
                newWalletHistories = walletHistoryData.walletHistories;
                adapter = new WalletHistoryAdapter(mContext, walletHistoryData.walletHistories);
                mView.loadData(adapter, walletHistoryData.amount, totalPage);
            }
        } else {
            mView.onGettingDataFailed();
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingWalletHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onGettingDataFailed();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
