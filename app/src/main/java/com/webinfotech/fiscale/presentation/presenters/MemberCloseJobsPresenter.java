package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.presentation.ui.adapters.MemberCloseJobsAdapter;

public interface MemberCloseJobsPresenter {
    void fetchMemberCloseJobs(int pageNo, String type);
    interface View {
        void loadAdapter(MemberCloseJobsAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
    }
}
