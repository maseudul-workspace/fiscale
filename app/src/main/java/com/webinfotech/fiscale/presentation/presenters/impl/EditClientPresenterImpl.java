package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.GetMemberClientDetailsInteractor;
import com.webinfotech.fiscale.domain.interactors.UpdateClientInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.GetMemberClientDetailsInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.UpdateClientInteractorImpl;
import com.webinfotech.fiscale.domain.models.MemberClientDetails;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.EditClientPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class EditClientPresenterImpl extends AbstractPresenter implements   EditClientPresenter,
                                                                            GetMemberClientDetailsInteractor.Callback,
                                                                            UpdateClientInteractor.Callback
{

    Context mContext;
    EditClientPresenter.View mView;
    GetMemberClientDetailsInteractorImpl getMemberClientDetailsInteractor;
    UpdateClientInteractorImpl updateClientInteractor;

    public EditClientPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getClientDetails(int id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getMemberClientDetailsInteractor = new GetMemberClientDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, id);
            getMemberClientDetailsInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void editClient(int id, int residentialAddressId, int businessAddressId, String name, String email, String mobile, String pan, String fatherName, String dob, String gender, String constitution, String tradeName, String flatNo, String village, String po, String ps, String area, String district, String state, String pin, String flatNoBusiness, String villageBusiness, String poBusiness, String psBusiness, String areaBusiness, String districtBusiness, String stateBusiness, String pinBusiness) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            updateClientInteractor = new UpdateClientInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, id, residentialAddressId, businessAddressId, name, email, mobile, pan, fatherName, dob, gender, constitution, tradeName, flatNo, village, po, ps, area, district, state, pin, flatNoBusiness, villageBusiness, poBusiness, psBusiness, areaBusiness, districtBusiness, stateBusiness, pinBusiness);
            updateClientInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingClientDetailsSuccess(MemberClientDetails clientDetails) {
        mView.loadClientDetails(clientDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingClientDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateUserSuccess() {
        Toasty.success(mContext, "Updated Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onUpdateUserFail(String errorMsg, int loginError) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }
}
