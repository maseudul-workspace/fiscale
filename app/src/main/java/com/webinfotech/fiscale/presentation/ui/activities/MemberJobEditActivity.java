package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.presentation.presenters.MemberJobEditPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.MemberJobEditPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.JobListAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

public class MemberJobEditActivity extends AppCompatActivity implements MemberJobEditPresenter.View {

    @BindView(R.id.txt_view_job_id)
    TextView txtViewJobId;
    @BindView(R.id.txt_view_client_name)
    TextView txtViewClientName;
    @BindView(R.id.txt_view_pan)
    TextView txtViewPan;
    @BindView(R.id.txt_view_mobile_no)
    TextView txtViewMobileNo;
    @BindView(R.id.txt_view_job_desc)
    TextView txtViewJobDesc;
    @BindView(R.id.txt_view_date)
    TextView txtViewDate;
    @BindView(R.id.recycler_view_jobs)
    RecyclerView recyclerViewJobs;
    @BindView(R.id.txt_view_job_name)
    TextView txtViewJobName;
    ProgressDialog progressDialog;
    MemberJobEditPresenterImpl mPresenter;
    int jobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_job_edit);
        ButterKnife.bind(this);
        jobId = getIntent().getIntExtra("jobId", 0);
        Log.e("LogMsg", "Job Id: " + jobId);
        initialisePresenter();
        setProgressDialog();
        mPresenter.getMemberJobEditDetails(jobId);
        mPresenter.getJobList();
    }

    private void initialisePresenter() {
        mPresenter = new MemberJobEditPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(JobSearchDetailsData jobSearchDetailsData) {
        txtViewClientName.setText(jobSearchDetailsData.clientName);
        txtViewDate.setText(jobSearchDetailsData.createdAt);
        txtViewJobDesc.setText(jobSearchDetailsData.jobDesc);
        txtViewJobId.setText(jobSearchDetailsData.jobId);
        txtViewJobName.setText(jobSearchDetailsData.jobTypeName);
        txtViewMobileNo.setText(jobSearchDetailsData.clientMobile);
        txtViewPan.setText(jobSearchDetailsData.clientPan);
    }

    @Override
    public void loadJobAdapter(JobListAdapter adapter) {
        recyclerViewJobs.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewJobs.setAdapter(adapter);
    }

    @Override
    public void setJobName(String jobName) {
        txtViewJobName.setText(jobName);
        recyclerViewJobs.setVisibility(View.GONE);
    }

    @Override
    public void onGettingDataFailed() {

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.job_layout) void onJobLayoutClicked() {
        if (recyclerViewJobs.getVisibility() == View.VISIBLE) {
            recyclerViewJobs.setVisibility(View.GONE);
        } else {
            recyclerViewJobs.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        mPresenter.updateJob();
    }

}
