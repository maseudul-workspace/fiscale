package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.CheckExecutiveLoginInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.CheckExecutiveLoginInteractorImpl;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.ExecutiveLoginPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ExecutiveLoginPresenterImpl extends AbstractPresenter implements ExecutiveLoginPresenter, CheckExecutiveLoginInteractor.Callback {

    Context mContext;
    ExecutiveLoginPresenter.View mView;
    CheckExecutiveLoginInteractorImpl checkExecutiveLoginInteractor;

    public ExecutiveLoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkExecutiveLogin(String email, String password) {
        checkExecutiveLoginInteractor = new CheckExecutiveLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkExecutiveLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideLoader();
        Toasty.success(mContext, "Login Successfully", Toast.LENGTH_SHORT).show();
        mView.goToJobTransactions();
    }

    @Override
    public void onLoginFailed(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
