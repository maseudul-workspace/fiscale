package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.presentation.ui.adapters.BranchClientListAdapter;

public interface BranchClientListPresenter {
    void fetchClientList(int page, String type);
    interface View {
        void loadAdapter(BranchClientListAdapter adapter, int totalPages);
        void goToClientDetails(String mobile);
        void goToClientDescription(int userId);
        void showLoader();
        void hideLoader();
    }
}
