package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.FetchMemberCloseJobsInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.FetchMemberCloseJobsInteractorImpl;
import com.webinfotech.fiscale.domain.models.CloseJobs;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.MemberCloseJobsPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.MemberCloseJobsAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberCloseJobsPresenterImpl extends AbstractPresenter implements MemberCloseJobsPresenter, FetchMemberCloseJobsInteractor.Callback {

    Context mContext;
    MemberCloseJobsPresenter.View mView;
    FetchMemberCloseJobsInteractorImpl fetchMemberCloseJobsInteractor;
    AndroidApplication androidApplication;
    CloseJobs[] newCloseJobs;
    MemberCloseJobsAdapter adapter;

    public MemberCloseJobsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMemberCloseJobs(int pageNo, String type) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            if (type.equals("refresh")) {
                newCloseJobs = null;
            }
            fetchMemberCloseJobsInteractor = new FetchMemberCloseJobsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.api_token, userInfo.id, pageNo);
            fetchMemberCloseJobsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingCloseJobsSuccess(CloseJobs[] closeJobs, int totalPage) {
        CloseJobs[] tempCloseJobs;
        tempCloseJobs = newCloseJobs;
        try {
            int len1 = tempCloseJobs.length;
            int len2 = closeJobs.length;
            newCloseJobs = new CloseJobs[len1 + len2];
            System.arraycopy(tempCloseJobs, 0, newCloseJobs, 0, len1);
            System.arraycopy(closeJobs, 0, newCloseJobs, len1, len2);
            adapter.updateDataSet(newCloseJobs);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newCloseJobs = closeJobs;
            adapter = new MemberCloseJobsAdapter(mContext, closeJobs);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingCloseJobFail(String errorMsg, int loginError) {

    }
}
