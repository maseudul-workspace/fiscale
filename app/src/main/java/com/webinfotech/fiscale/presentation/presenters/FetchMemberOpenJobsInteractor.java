package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.OpenJobs;

public interface FetchMemberOpenJobsInteractor {
    interface Callback {
        void onGettingMemberOpenJobsSuccess(OpenJobs[] openJobs, int totalPage);
        void onGettingMemberOpenJobsFail(String errorMsg, int loginError);
    }
}
