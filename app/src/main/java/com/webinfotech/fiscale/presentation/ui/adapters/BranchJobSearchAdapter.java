package com.webinfotech.fiscale.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.models.Testing.JobSearch;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BranchJobSearchAdapter extends RecyclerView.Adapter<BranchJobSearchAdapter.ViewHolder> {

    Context mContext;
    ArrayList<JobSearch> jobSearchArrayList;

    public BranchJobSearchAdapter(Context mContext, ArrayList<JobSearch> jobSearchArrayList) {
        this.mContext = mContext;
        this.jobSearchArrayList = jobSearchArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_branch_job_search, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        holder.txtViewDate.setText(jobSearchArrayList.get(position).date);
        holder.txtViewCommentedBy.setText(jobSearchArrayList.get(position).commentedBy);
        holder.txtViewRemarks.setText(jobSearchArrayList.get(position).remarks);
    }

    @Override
    public int getItemCount() {
        return jobSearchArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_commented_by)
        TextView txtViewCommentedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

