package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.Testing.MainJobs;
import com.webinfotech.fiscale.presentation.presenters.MemberOpenJobPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.MemberOpenJobPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.MainJobsAdapter;
import com.webinfotech.fiscale.presentation.ui.adapters.MemberOpenJobsAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.ArrayList;

public class EmployeeOpenJobsActivity extends EmployeeBaseActivity implements MemberOpenJobPresenter.View {

    @BindView(R.id.recycler_view_open_jobs)
    RecyclerView recyclerViewOpenJobs;
    MemberOpenJobPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_employee_open_jobs);
        getSupportActionBar().setTitle("Open Jobs");
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchOpenJobs(pageNo, "refresh");
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new MemberOpenJobPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(MemberOpenJobsAdapter openJobsAdapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewOpenJobs.setAdapter(openJobsAdapter);
        recyclerViewOpenJobs.setLayoutManager(layoutManager);
        recyclerViewOpenJobs.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchOpenJobs(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void goToJobDetails(String jobId, int id) {
        Intent intent = new Intent(this, MemberJobDetailsActivity.class);
        intent.putExtra("jobId", jobId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
