package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.Testing.BranchUser;
import com.webinfotech.fiscale.presentation.presenters.BranchClientListPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.BranchClientListPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.BranchClientListAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.ArrayList;

public class BranchClientListActivity extends BranchBaseActivity implements BranchClientListPresenter.View {

    @BindView(R.id.recycler_view_branch_users)
    RecyclerView recyclerViewBranchUsers;
    ProgressDialog progressDialog;
    BranchClientListPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_branch_user_list);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        mPresenter.fetchClientList(pageNo, "refresh");
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new BranchClientListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(BranchClientListAdapter adapter, int totalPages) {
        this.totalPage = totalPages;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewBranchUsers.setAdapter(adapter);
        recyclerViewBranchUsers.setLayoutManager(layoutManager);
        recyclerViewBranchUsers.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchClientList(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void goToClientDetails(String mobile) {
        Intent intent = new Intent(this, ClientDetailsActivity.class);
        intent.putExtra("searchKey", mobile);
        startActivity(intent);
    }

    @Override
    public void goToClientDescription(int userId) {
        Intent intent = new Intent(this, ClientDescriptionActivity.class);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
