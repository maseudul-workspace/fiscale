package com.webinfotech.fiscale.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fiscale.AndroidApplication;
import com.webinfotech.fiscale.domain.executors.Executor;
import com.webinfotech.fiscale.domain.executors.MainThread;
import com.webinfotech.fiscale.domain.interactors.SearchMemberClientInteractor;
import com.webinfotech.fiscale.domain.interactors.impl.SearchClientWithJobsInteractorImpl;
import com.webinfotech.fiscale.domain.interactors.impl.SearchMemberClientInteractorImpl;
import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.ClientSearchData;
import com.webinfotech.fiscale.domain.models.UserInfo;
import com.webinfotech.fiscale.presentation.presenters.MemberSearchClientPresenter;
import com.webinfotech.fiscale.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fiscale.presentation.ui.adapters.SpClientSearchJobListAdapter;
import com.webinfotech.fiscale.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MemberSearchClientPresenterImpl extends AbstractPresenter implements   MemberSearchClientPresenter,
                                                                                    SearchMemberClientInteractor.Callback,
                                                                                    SpClientSearchJobListAdapter.Callback
{

    Context mContext;
    MemberSearchClientPresenter.View mView;
    SearchMemberClientInteractorImpl searchMemberClientInteractor;
    SpClientSearchJobListAdapter adapter;

    public MemberSearchClientPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void searchClient(String searchKey) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your session is expired!!! Please login again", Toast.LENGTH_SHORT).show();
        } else {
            searchMemberClientInteractor = new SearchMemberClientInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.api_token, searchKey);
            searchMemberClientInteractor.execute();
            mView.showLoader();
        }
    }


    @Override
    public void onSearchClientSuccess(ClientSearchData clientSearchData) {
        adapter = new SpClientSearchJobListAdapter(mContext, clientSearchData.jobLists, this);
        mView.loadClientData(clientSearchData, adapter);
        mView.hideLoader();
    }

    @Override
    public void onSearchClientFail(String errorMsg, int loginError) {

    }

    @Override
    public void onViewJobClicked(String jobId, int id) {
        mView.goToJobDetailsActivity(jobId, id);
    }
}
