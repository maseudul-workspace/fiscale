package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.MemberClientDetails;
import com.webinfotech.fiscale.presentation.presenters.EditClientPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.EditClientPresenterImpl;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.Calendar;

public class EditClientActivity extends AppCompatActivity implements EditClientPresenter.View {

    @BindView(R.id.spinner_constitution)
    Spinner spinnerConstitution;
    @BindView(R.id.spinner_gender)
    Spinner spinnerGender;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_fathers_name)
    EditText editTextFathersName;
    @BindView(R.id.txt_input_pan_layout)
    TextInputLayout txtInputPanLayout;
    @BindView(R.id.edit_text_pan)
    EditText editTextPan;
    @BindView(R.id.txt_input_mobile_layout)
    TextInputLayout txtInputMobileLayout;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.edit_text_flat_no)
    EditText editTextFlatNo;
    @BindView(R.id.txt_input_building_layout)
    TextInputLayout txtInputBuildingLayout;
    @BindView(R.id.edit_text_building_no)
    EditText editTextBuildingNo;
    @BindView(R.id.txt_input_po_layout)
    TextInputLayout txtInputPOLayout;
    @BindView(R.id.edit_text_po)
    EditText editTextPO;
    @BindView(R.id.txt_input_ps_layout)
    TextInputLayout txtInputPSLayout;
    @BindView(R.id.edit_text_ps)
    EditText editTextPS;
    @BindView(R.id.edit_text_area)
    EditText editTextArea;
    @BindView(R.id.txt_input_district_layout)
    TextInputLayout txtInputDistrictLayout;
    @BindView(R.id.edit_text_district)
    EditText editTextDistrict;
    @BindView(R.id.txt_input_state_layout)
    TextInputLayout txtInputStateLayout;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.txt_input_pin_layout)
    TextInputLayout txtInputPinLayout;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_business_flat_no)
    EditText editTextBusinessFlatNo;
    @BindView(R.id.txt_input_business_building_layout)
    TextInputLayout txtInputBusinessBuildingLayout;
    @BindView(R.id.edit_text_business_building_no)
    EditText editTextBusinessBuildingNo;
    @BindView(R.id.txt_input_business_po_layout)
    TextInputLayout txtInputBusinessPOLayout;
    @BindView(R.id.edit_text_business_po)
    EditText editTextBusinessPO;
    @BindView(R.id.txt_input_business_ps_layout)
    TextInputLayout txtInputBusinessPSLayout;
    @BindView(R.id.edit_text_business_ps)
    EditText editTextBusinessPS;
    @BindView(R.id.edit_text_business_area)
    EditText editTextBusinessArea;
    @BindView(R.id.txt_input_business_district_layout)
    TextInputLayout txtInputBusinessDistrictLayout;
    @BindView(R.id.edit_text_business_district)
    EditText editTextBusinessDistrict;
    @BindView(R.id.txt_input_business_state_layout)
    TextInputLayout txtInputBusinesStateLayout;
    @BindView(R.id.edit_text_business_state)
    EditText editTextBusinessState;
    @BindView(R.id.txt_input_business_pin_layout)
    TextInputLayout txtInputBusinessPinLayout;
    @BindView(R.id.edit_text_business_pin)
    EditText editTextBusinessPin;
    @BindView(R.id.edit_text_trade)
    EditText editTextTrade;
    EditClientPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int userId;
    String[] constitutionList = {
            "Select Constitution From The List",
            "Individual",
            "Company",
            "Others"
    };

    String[] genderList = {
            "Select Gender",
            "Male",
            "Female"
    };
    String constitution;
    String gender;
    int residentialAddressId;
    int businessAddressId;
    String dob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_client);
        userId = getIntent().getIntExtra("clientId", 0);
        ButterKnife.bind(this);
        setSpinnerConstitution();
        setSpinnerGender();
        initialisePresenter();
        setProgressDialog();
        mPresenter.getClientDetails(userId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new EditClientPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.txt_view_dob) void onDobClicked() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDob.setText(dob);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    public void setSpinnerConstitution() {
        final ArrayAdapter<String> constitutionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, constitutionList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    constitution = constitutionList[position];
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        constitutionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerConstitution.setAdapter(constitutionAdapter);
    }

    public void setSpinnerGender() {
        final ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    if (genderList[position].equals("Male")) {
                        gender = "M";
                    } else {
                        gender = "F";
                    }
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(genderAdapter);
    }

    @Override
    public void loadClientDetails(MemberClientDetails clientDetails) {
        editTextArea.setText(clientDetails.residentialAddress.area);
        editTextBuildingNo.setText(clientDetails.residentialAddress.village);
        editTextBusinessArea.setText(clientDetails.businessAddress.area);
        editTextBusinessBuildingNo.setText(clientDetails.businessAddress.village);
        editTextBusinessDistrict.setText(clientDetails.businessAddress.dist);
        editTextBusinessFlatNo.setText(clientDetails.businessAddress.flatNo);
        editTextBusinessPin.setText(clientDetails.businessAddress.pin);
        editTextBusinessPO.setText(clientDetails.businessAddress.po);
        editTextBusinessPS.setText(clientDetails.businessAddress.ps);
        editTextBusinessState.setText(clientDetails.businessAddress.state);
        editTextDistrict.setText(clientDetails.residentialAddress.dist);
        editTextEmail.setText(clientDetails.email);
        editTextFathersName.setText(clientDetails.fatherName);
        editTextFlatNo.setText(clientDetails.residentialAddress.flatNo);
        editTextMobile.setText(clientDetails.mobile);
        editTextName.setText(clientDetails.name);
        editTextPan.setText(clientDetails.pan);
        editTextPin.setText(clientDetails.residentialAddress.pin);
        editTextPO.setText(clientDetails.residentialAddress.po);
        editTextPS.setText(clientDetails.residentialAddress.ps);
        editTextState.setText(clientDetails.residentialAddress.state);
        editTextTrade.setText(clientDetails.tradeName);
        if (clientDetails.gender.equals("M")) {
            spinnerGender.setSelection(1);
        } else {
            spinnerGender.setSelection(2);
        }
        if (clientDetails.constitution.equals("Individual")) {
            spinnerConstitution.setSelection(1);
        } else if (clientDetails.constitution.equals("Company")) {
            spinnerConstitution.setSelection(2);
        } else {
            spinnerConstitution.setSelection(3);
        }
        residentialAddressId = clientDetails.residentialAddressId;
        businessAddressId = clientDetails.businessAddressId;
        gender = clientDetails.gender;
        constitution = clientDetails.constitution;
        txtViewDob.setText(clientDetails.dob);
        dob = clientDetails.dob;
    }

    @OnClick(R.id.btn_update) void onUpdateClicked() {
        txtInputNameLayout.setError("");
        txtInputMobileLayout.setError("");
        txtInputPanLayout.setError("");
        txtInputBuildingLayout.setError("");
        txtInputBuildingLayout.setError("");
        txtInputPOLayout.setError("");
        txtInputPSLayout.setError("");
        txtInputDistrictLayout.setError("");
        txtInputStateLayout.setError("");
        txtInputPinLayout.setError("");
        txtInputBusinessBuildingLayout.setError("");
        txtInputBusinessPOLayout.setError("");
        txtInputBusinessPSLayout.setError("");
        txtInputBusinessDistrictLayout.setError("");
        txtInputBusinesStateLayout.setError("");
        txtInputBusinessPinLayout.setError("");
        if (editTextName.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Name Required");
            txtInputNameLayout.requestFocus();
        } else if (editTextMobile.getText().toString().trim().isEmpty()) {
            txtInputMobileLayout.setError("Mobile required");
            txtInputMobileLayout.requestFocus();
        } else if (editTextPan.getText().toString().trim().isEmpty()) {
            txtInputPanLayout.setError("Pan No Required");
            txtInputPanLayout.requestFocus();
        } else if (gender == null) {
            Toasty.warning(this, "Please Enter gender", Toast.LENGTH_SHORT).show();
        } else if (constitution == null) {
            Toasty.warning(this, "Please Enter Constitution", Toast.LENGTH_SHORT).show();
        } else if (dob == null) {
            Toasty.warning(this, "Please Enter Date of Birth", Toast.LENGTH_SHORT).show();
        } else if (editTextBuildingNo.getText().toString().isEmpty()) {
            txtInputBuildingLayout.setError("Building/Village required");
            txtInputBuildingLayout.requestFocus();
        } else if (editTextPO.getText().toString().isEmpty()) {
            txtInputPOLayout.setError("Post office required");
            txtInputPOLayout.requestFocus();
        } else if (editTextPS.getText().toString().isEmpty()) {
            txtInputPSLayout.setError("Police station required");
            txtInputPSLayout.requestFocus();
        } else if (editTextDistrict.getText().toString().isEmpty()) {
            txtInputDistrictLayout.setError("District required");
            txtInputDistrictLayout.requestFocus();
        } else if (editTextState.getText().toString().isEmpty()) {
            txtInputStateLayout.setError("State required");
            txtInputStateLayout.requestFocus();
        } else if (editTextPin.getText().toString().isEmpty()) {
            txtInputPinLayout.setError("Pin required");
            txtInputPinLayout.requestFocus();
        } else if (editTextBusinessBuildingNo.getText().toString().isEmpty()) {
            txtInputBusinessBuildingLayout.setError("Building No required");
            txtInputBusinessBuildingLayout.requestFocus();
        } else if (editTextBusinessPO.getText().toString().isEmpty()) {
            txtInputBusinessPOLayout.setError("PO required");
            txtInputBusinessPOLayout.requestFocus();
        } else if (editTextBusinessPS.getText().toString().isEmpty()) {
            txtInputBusinessPSLayout.setError("PS required");
            txtInputBusinessPSLayout.requestFocus();
        } else if (editTextBusinessDistrict.getText().toString().isEmpty()) {
            txtInputBusinessDistrictLayout.setError("District required");
            txtInputBusinessDistrictLayout.requestFocus();
        } else if (editTextBusinessState.getText().toString().isEmpty()) {
            txtInputBusinesStateLayout.setError("State required");
            txtInputBusinesStateLayout.requestFocus();
        } else if (editTextBusinessPin.getText().toString().isEmpty()) {
            txtInputBusinessPinLayout.setError("Pin required");
            txtInputBusinessPinLayout.requestFocus();
        } else {
            mPresenter.editClient(
                    userId,
                    residentialAddressId,
                    businessAddressId,
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextMobile.getText().toString(),
                    editTextPan.getText().toString(),
                    editTextFathersName.getText().toString(),
                    dob,
                    gender,
                    constitution,
                    editTextTrade.getText().toString(),
                    editTextFlatNo.getText().toString(),
                    editTextBuildingNo.getText().toString(),
                    editTextPO.getText().toString(),
                    editTextPS.getText().toString(),
                    editTextArea.getText().toString(),
                    editTextDistrict.getText().toString(),
                    editTextState.getText().toString(),
                    editTextPin.getText().toString(),
                    editTextBusinessFlatNo.getText().toString(),
                    editTextBusinessBuildingNo.getText().toString(),
                    editTextBusinessPO.getText().toString(),
                    editTextBusinessPS.getText().toString(),
                    editTextBusinessArea.getText().toString(),
                    editTextBusinessDistrict.getText().toString(),
                    editTextBusinessState.getText().toString(),
                    editTextBusinessPin.getText().toString()
            );
        }
    }

    @Override
    public void onEditSuccess() {

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
