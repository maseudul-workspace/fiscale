package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.presentation.ui.adapters.JobListAdapter;

public interface MemberJobEditPresenter {
    void getMemberJobEditDetails(int jobId);
    void getJobList();
    void updateJob();
    interface View {
        void loadData(JobSearchDetailsData jobSearchDetailsData);
        void loadJobAdapter(JobListAdapter adapter);
        void setJobName(String jobName);
        void onGettingDataFailed();
        void showLoader();
        void hideLoader();

    }
}
