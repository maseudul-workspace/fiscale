package com.webinfotech.fiscale.presentation.presenters;

import com.webinfotech.fiscale.domain.models.ClientList;
import com.webinfotech.fiscale.domain.models.JobDescList;

import java.util.ArrayList;

public interface BranchAddJobPresenter {
    void fetchClientDetails(String searchKey);
    void fetchJobDescList();
    void addClientExistingJobs(ArrayList<Integer> jobIds, int clientId);
    interface View {
        void loadClientDetails(ClientList clientList);
        void loadJobDescList(JobDescList[] jobDescLists);
        void onLoadClientDetailsFail();
        void hideLoader();
        void showLoader();
    }
}
