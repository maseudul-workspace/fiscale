package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;

import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.Testing.MainJobs;
import com.webinfotech.fiscale.presentation.presenters.OpenJobsPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.OpenJobsPresenterImpl;
import com.webinfotech.fiscale.presentation.ui.adapters.CorrectionAdapter;
import com.webinfotech.fiscale.presentation.ui.adapters.OpenJobsAdapter;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.ArrayList;

public class BranchOpenJobsActivity extends BranchBaseActivity implements OpenJobsPresenter.View {

    @BindView(R.id.recycler_view_correction_jobs)
    RecyclerView recyclerViewCorrectionJobs;
    ProgressDialog progressDialog;
    OpenJobsPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_branch_open_jobs);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        mPresenter.fetchOpenJobs(pageNo, "refresh");
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new OpenJobsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(OpenJobsAdapter openJobsAdapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewCorrectionJobs.setAdapter(openJobsAdapter);
        recyclerViewCorrectionJobs.setLayoutManager(layoutManager);
        recyclerViewCorrectionJobs.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchOpenJobs(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void goToJobDetails(String jobId, int id) {
        Intent intent = new Intent(this, SpClientJobDetailsActivity.class);
        intent.putExtra("jobId", jobId);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}
