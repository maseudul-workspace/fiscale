package com.webinfotech.fiscale.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.fiscale.R;
import com.webinfotech.fiscale.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.presentation.presenters.SpAddClientPresenter;
import com.webinfotech.fiscale.presentation.presenters.impl.SpAddClientPresenterImpl;
import com.webinfotech.fiscale.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.Calendar;

public class BranchAddClientActivity extends BranchBaseActivity implements SpAddClientPresenter.View {

    @BindView(R.id.layout_job_description)
    LinearLayout layoutJobDescription;
    @BindView(R.id.layout_grey_step2)
    View layoutGreyStep2;
    @BindView(R.id.layout_grey_step3)
    View getLayoutGreyStep3;
    @BindView(R.id.layout_yellow_step1)
    View layoutYellowStep1;
    @BindView(R.id.layout_yellow_step2)
    View layoutYellowStep2;
    @BindView(R.id.layout_yellow_step3)
    View layoutYellowStep3;
    @BindView(R.id.layout_1)
    View layout1;
    @BindView(R.id.layout_2)
    View layout2;
    @BindView(R.id.layout_3)
    View layout3;
    @BindView(R.id.spinner_constitution)
    Spinner spinnerConstitution;
    @BindView(R.id.spinner_gender)
    Spinner spinnerGender;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_fathers_name)
    EditText editTextFathersName;
    @BindView(R.id.txt_input_pan_layout)
    TextInputLayout txtInputPanLayout;
    @BindView(R.id.edit_text_pan)
    EditText editTextPan;
    @BindView(R.id.txt_input_mobile_layout)
    TextInputLayout txtInputMobileLayout;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDob;
    @BindView(R.id.edit_text_flat_no)
    EditText editTextFlatNo;
    @BindView(R.id.txt_input_building_layout)
    TextInputLayout txtInputBuildingLayout;
    @BindView(R.id.edit_text_building_no)
    EditText editTextBuildingNo;
    @BindView(R.id.txt_input_po_layout)
    TextInputLayout txtInputPOLayout;
    @BindView(R.id.edit_text_po)
    EditText editTextPO;
    @BindView(R.id.txt_input_ps_layout)
    TextInputLayout txtInputPSLayout;
    @BindView(R.id.edit_text_ps)
    EditText editTextPS;
    @BindView(R.id.edit_text_area)
    EditText editTextArea;
    @BindView(R.id.txt_input_district_layout)
    TextInputLayout txtInputDistrictLayout;
    @BindView(R.id.edit_text_district)
    EditText editTextDistrict;
    @BindView(R.id.txt_input_state_layout)
    TextInputLayout txtInputStateLayout;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.txt_input_pin_layout)
    TextInputLayout txtInputPinLayout;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_business_flat_no)
    EditText editTextBusinessFlatNo;
    @BindView(R.id.txt_input_business_building_layout)
    TextInputLayout txtInputBusinessBuildingLayout;
    @BindView(R.id.edit_text_business_building_no)
    EditText editTextBusinessBuildingNo;
    @BindView(R.id.txt_input_business_po_layout)
    TextInputLayout txtInputBusinessPOLayout;
    @BindView(R.id.edit_text_business_po)
    EditText editTextBusinessPO;
    @BindView(R.id.txt_input_business_ps_layout)
    TextInputLayout txtInputBusinessPSLayout;
    @BindView(R.id.edit_text_business_ps)
    EditText editTextBusinessPS;
    @BindView(R.id.edit_text_business_area)
    EditText editTextBusinessArea;
    @BindView(R.id.txt_input_business_district_layout)
    TextInputLayout txtInputBusinessDistrictLayout;
    @BindView(R.id.edit_text_business_district)
    EditText editTextBusinessDistrict;
    @BindView(R.id.txt_input_business_state_layout)
    TextInputLayout txtInputBusinesStateLayout;
    @BindView(R.id.edit_text_business_state)
    EditText editTextBusinessState;
    @BindView(R.id.txt_input_business_pin_layout)
    TextInputLayout txtInputBusinessPinLayout;
    @BindView(R.id.edit_text_business_pin)
    EditText editTextBusinessPin;
    @BindView(R.id.edit_text_trade)
    EditText editTextTrade;
    @BindView(R.id.checkbox_address)
    CheckBox addressCheckbox;
    boolean isDateSelected = false;
    String dob;
    String[] constitutionList = {
            "Select Constitution From The List",
            "Individual",
            "Company",
            "Others"
    };

    String[] genderList = {
            "Select Gender",
            "Male",
            "Female"
    };
    SpAddClientPresenterImpl mPresenter;
    String constitution;
    String gender;
    JobDescList[] jobDescLists;
    ArrayList<Integer> jobIds = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_branch_add_client);
        ButterKnife.bind(this);
        setSpinnerConstitution();
        setSpinnerGender();
        setAddressCheckbox();
        setProgressDialog();
        initialisePresenter();
        mPresenter.fetchJobDescList();
    }

    public void initialisePresenter() {
        mPresenter = new SpAddClientPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setSpinnerConstitution() {
        final ArrayAdapter<String> constitutionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, constitutionList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    constitution = constitutionList[position];
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        constitutionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerConstitution.setAdapter(constitutionAdapter);
    }

    public void setSpinnerGender() {
        final ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    if (genderList[position].equals("Male")) {
                        gender = "M";
                    } else {
                        gender = "F";
                    }
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(genderAdapter);
    }

    private void setAddressCheckbox() {
        addressCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editTextBusinessArea.setText(editTextArea.getText().toString());
                    editTextBusinessBuildingNo.setText(editTextBuildingNo.getText().toString());
                    editTextBusinessDistrict.setText(editTextDistrict.getText().toString());
                    editTextBusinessFlatNo.setText(editTextFlatNo.getText().toString());
                    editTextBusinessPin.setText(editTextPin.getText().toString());
                    editTextBusinessPO.setText(editTextPO.getText().toString());
                    editTextBusinessPS.setText(editTextPS.getText().toString());
                    editTextBusinessState.setText(editTextState.getText().toString());
                } else {
                    editTextBusinessArea.setText("");
                    editTextBusinessBuildingNo.setText("");
                    editTextBusinessDistrict.setText("");
                    editTextBusinessFlatNo.setText("");
                    editTextBusinessPin.setText("");
                    editTextBusinessPO.setText("");
                    editTextBusinessPS.setText("");
                    editTextBusinessState.setText("");
                }
            }
        });
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_layout_1_next) void onBtnLayout1NextClicked() {
        txtInputNameLayout.setError("");
        txtInputMobileLayout.setError("");
        txtInputPanLayout.setError("");
        if (editTextName.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Name Required");
            txtInputNameLayout.requestFocus();
        } else if (editTextMobile.getText().toString().trim().isEmpty()) {
            txtInputMobileLayout.setError("Mobile required");
            txtInputMobileLayout.requestFocus();
        } else if (editTextPan.getText().toString().trim().isEmpty()) {
            txtInputPanLayout.setError("Pan No Required");
            txtInputPanLayout.requestFocus();
        } else if (gender == null) {
            Toasty.warning(this, "Please Enter gender", Toast.LENGTH_SHORT).show();
        } else if (constitution == null) {
            Toasty.warning(this, "Please Enter Constitution", Toast.LENGTH_SHORT).show();
        } else if (dob == null) {
            Toasty.warning(this, "Please Enter Date of Birth", Toast.LENGTH_SHORT).show();
        } else {
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.VISIBLE);
            layout3.setVisibility(View.GONE);
            layoutGreyStep2.setVisibility(View.GONE);
            layoutYellowStep2.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_layout_2_next) void onBtnLayout2NextClicked() {
        txtInputBuildingLayout.setError("");
        txtInputPOLayout.setError("");
        txtInputPSLayout.setError("");
        txtInputDistrictLayout.setError("");
        txtInputStateLayout.setError("");
        txtInputPinLayout.setError("");
        txtInputBusinessBuildingLayout.setError("");
        txtInputBusinessPOLayout.setError("");
        txtInputBusinessPSLayout.setError("");
        txtInputBusinessDistrictLayout.setError("");
        txtInputBusinesStateLayout.setError("");
        txtInputBusinessPinLayout.setError("");
        if (editTextBuildingNo.getText().toString().isEmpty()) {
            txtInputBuildingLayout.setError("Building/Village required");
            txtInputBuildingLayout.requestFocus();
        } else if (editTextPO.getText().toString().isEmpty()) {
            txtInputPOLayout.setError("Post office required");
            txtInputPOLayout.requestFocus();
        } else if (editTextPS.getText().toString().isEmpty()) {
            txtInputPSLayout.setError("Police station required");
            txtInputPSLayout.requestFocus();
        } else if (editTextDistrict.getText().toString().isEmpty()) {
            txtInputDistrictLayout.setError("District required");
            txtInputDistrictLayout.requestFocus();
        } else if (editTextState.getText().toString().isEmpty()) {
            txtInputStateLayout.setError("State required");
            txtInputStateLayout.requestFocus();
        } else if (editTextPin.getText().toString().isEmpty()) {
            txtInputPinLayout.setError("Pin required");
            txtInputPinLayout.requestFocus();
        } else if (editTextBusinessBuildingNo.getText().toString().isEmpty()) {
            txtInputBusinessBuildingLayout.setError("Building No required");
            txtInputBusinessBuildingLayout.requestFocus();
        } else if (editTextBusinessPO.getText().toString().isEmpty()) {
            txtInputBusinessPOLayout.setError("PO required");
            txtInputBusinessPOLayout.requestFocus();
        } else if (editTextBusinessPS.getText().toString().isEmpty()) {
            txtInputBusinessPSLayout.setError("PS required");
            txtInputBusinessPSLayout.requestFocus();
        } else if (editTextBusinessDistrict.getText().toString().isEmpty()) {
            txtInputBusinessDistrictLayout.setError("District required");
            txtInputBusinessDistrictLayout.requestFocus();
        } else if (editTextBusinessState.getText().toString().isEmpty()) {
            txtInputBusinesStateLayout.setError("State required");
            txtInputBusinesStateLayout.requestFocus();
        } else if (editTextBusinessPin.getText().toString().isEmpty()) {
            txtInputBusinessPinLayout.setError("Pin required");
            txtInputBusinessPinLayout.requestFocus();
        } else {
            Log.e("LogMsg", "Active");
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.VISIBLE);
            getLayoutGreyStep3.setVisibility(View.GONE);
            layoutYellowStep3.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_layout_2_prev) void onBtnLayout2PrevClicked() {
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.GONE);
        layout3.setVisibility(View.GONE);
        layoutGreyStep2.setVisibility(View.VISIBLE);
        layoutYellowStep2.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_layout_3_prev) void onBtnLayout3PrevClicked() {
        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.VISIBLE);
        layout3.setVisibility(View.GONE);
        getLayoutGreyStep3.setVisibility(View.VISIBLE);
        layoutYellowStep3.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_dob) void onDobClicked() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        isDateSelected = true;
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDob.setText(dob);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    @Override
    public void loadJobDescList(JobDescList[] jobDescList) {
        this.jobDescLists = jobDescList;
        for (int i = 0; i < jobDescList.length; i++) {
            View customLayout = LayoutInflater.from(this).inflate(R.layout.layout_job_description, null, false);
            layoutJobDescription.addView(customLayout);
            CheckBox checkBox = (CheckBox) customLayout.findViewById(R.id.job_checkbox);
            checkBox.setText(jobDescList[i].name);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        for (int j = 0; j < jobDescList.length; j++) {
                            if (checkBox.getText().toString().equals(jobDescList[j].name)) {
                                jobIds.add(jobDescList[j].id);
                                break;
                            }
                        }
                    } else {
                        for (int j = 0; j < jobDescList.length; j++) {
                            if (checkBox.getText().toString().equals(jobDescList[j].name)) {
                                removeJobIds(jobDescList[j].id);
                                break;
                            }
                        }
                    }
                }
            });
        }
    }

    @OnClick(R.id.btn_layout_3_submit) void onSubmitClicked() {
        mPresenter.registerClient(
                editTextName.getText().toString(),
                editTextEmail.getText().toString(),
                editTextMobile.getText().toString(),
                editTextPan.getText().toString(),
                editTextFathersName.getText().toString(),
                dob,
                gender,
                constitution,
                editTextTrade.getText().toString(),
                editTextFlatNo.getText().toString(),
                editTextBuildingNo.getText().toString(),
                editTextPO.getText().toString(),
                editTextPS.getText().toString(),
                editTextArea.getText().toString(),
                editTextDistrict.getText().toString(),
                editTextState.getText().toString(),
                editTextPin.getText().toString(),
                editTextBusinessFlatNo.getText().toString(),
                editTextBusinessBuildingNo.getText().toString(),
                editTextBusinessPO.getText().toString(),
                editTextBusinessPS.getText().toString(),
                editTextBusinessArea.getText().toString(),
                editTextBusinessDistrict.getText().toString(),
                editTextBusinessState.getText().toString(),
                editTextBusinessPin.getText().toString(),
                jobIds
        );
        showLoader();
    }

    private void removeJobIds(int id) {
        for (int i = 0; i < jobIds.size(); i++) {
            if (jobIds.get(i) == id) {
                jobIds.remove(i);
                break;
            }
        }
    }

    @Override
    public void onRegisterSuccess() {
        editTextBusinessPin.setText("");
        editTextBusinessState.setText("");
        editTextBusinessDistrict.setText("");
        editTextBusinessPS.setText("");
        editTextBusinessPO.setText("");
        editTextBusinessBuildingNo.setText("");
        editTextPin.setText("");
        editTextState.setText("");
        editTextDistrict.setText("");
        editTextPS.setText("");
        editTextPO.setText("");
        editTextPan.setText("");
        editTextMobile.setText("");
        editTextName.setText("");
        editTextArea.setText("");
        editTextEmail.setText("");
        editTextFathersName.setText("");
        editTextFlatNo.setText("");
        editTextTrade.setText("");
        layoutYellowStep2.setVisibility(View.GONE);
        layoutYellowStep3.setVisibility(View.GONE);
        layoutGreyStep2.setVisibility(View.VISIBLE);
        getLayoutGreyStep3.setVisibility(View.VISIBLE);
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.VISIBLE);
        layout3.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}