package com.webinfotech.fiscale.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface AppRepository {

    @POST("sp/login")
    @FormUrlEncoded
    Call<ResponseBody> checkSPlogin(@Field("email") String email,
                                    @Field("password") String password
    );

    @GET("job/desc/list")
    Call<ResponseBody> getJobDescList();

    @POST("sp/client/registration")
    @FormUrlEncoded
    Call<ResponseBody> registerClient(@Header("Authorization") String authorization,
                                      @Field("name") String name,
                                      @Field("email") String email,
                                      @Field("mobile") String mobile,
                                      @Field("pan") String pan,
                                      @Field("father_name") String fatherName,
                                      @Field("dob") String dob,
                                      @Field("gender") String gender,
                                      @Field("constitution") String constitution,
                                      @Field("trade_name") String tradeName,
                                      @Field("created_by_id") int createdById,
                                      @Field("flat_no") String flatNo,
                                      @Field("village") String village,
                                      @Field("po") String po,
                                      @Field("ps") String ps,
                                      @Field("area") String area,
                                      @Field("district") String district,
                                      @Field("state") String state,
                                      @Field("pin") String pin,
                                      @Field("flat_no_b") String flatNoBusiness,
                                      @Field("village_b") String villageBusiness,
                                      @Field("po_b") String poBusiness,
                                      @Field("ps_b") String psBusiness,
                                      @Field("area_b") String areaBusiness,
                                      @Field("district_b") String districtBusiness,
                                      @Field("state_b") String stateBusiness,
                                      @Field("pin_b") String pinBusiness,
                                      @Field("job_type[]") ArrayList<Integer> jobIds
                                      );

    @GET("sp/client/list/{sp_id}/{page}")
    Call<ResponseBody> getClientList(@Header("Authorization") String authorization,
                                     @Path("sp_id") int spId,
                                     @Path("page") int page
                                     );

    @GET("sp/job/add/client/search/{search_key}")
    Call<ResponseBody> searchClient(@Header("Authorization") String authorization,
                                    @Path("search_key") String searchKey
    );

    @POST("sp/job/add/existing/client")
    @FormUrlEncoded
    Call<ResponseBody> addExistingClientJob(@Header("Authorization") String authorization,
                                            @Field("job_type[]") ArrayList<Integer> jobIds,
                                            @Field("client_id") int clientId,
                                            @Field("sp_id") int spId
    );

    @GET("sp/client/search/{search_key}")
    Call<ResponseBody> searchClientWithJobs(@Header("Authorization") String authorization,
                                            @Path("search_key") String searchKey
    );

    @GET("sp/job/search/{job_id}")
    Call<ResponseBody> searchClientJobDetails(@Header("Authorization") String authorization,
                                              @Path("job_id") String job_id
    );

    @POST("sp/add/job/remarks")
    @FormUrlEncoded
    Call<ResponseBody> addJobRemarks( @Header("Authorization") String authorization,
                                      @Field("sp_id") int sp_id,
                                      @Field("job_id") int job_id,
                                      @Field("message") String message
    );

    @GET("sp/client/details/{id}")
    Call<ResponseBody> fetchClientDetails(@Header("Authorization") String authorization,
                                          @Path("id") int userId
    );

    @GET("sp/open/jobs/{sp_id}/{page_no}")
    Call<ResponseBody> fetchOpenJobs(@Header("Authorization") String authorization,
                                     @Path("sp_id") int spId,
                                     @Path("page_no") int pageNo
    );

    @POST("member/login")
    @FormUrlEncoded
    Call<ResponseBody> checkMemberlogin(@Field("email") String email,
                                        @Field("password") String password
    );

    @GET("member/open/jobs/{member_id}/{page_no}")
    Call<ResponseBody> fetchMemberOpenJobs(  @Header("Authorization") String authorization,
                                             @Path("member_id") int memberId,
                                             @Path("page_no") int pageNo
    );

    @GET("member/details/job/{job_id}")
    Call<ResponseBody> fetchMemberJobDetails(  @Header("Authorization") String authorization,
                                               @Path("job_id") String jobId
    );

    @GET("member/reject/job/{id}")
    Call<ResponseBody> rejectJob( @Header("Authorization") String authorization,
                                  @Path("id") int jobId
    );

    @GET("member/search/client/{search_key}")
    Call<ResponseBody> searchMemberClient(@Header("Authorization") String authorization,
                                                  @Path("search_key") String searchKey
    );

    @GET("member/job/edit/details/{id}")
    Call<ResponseBody> getJobEditDetails(@Header("Authorization") String authorization,
                                      @Path("id") int jobId
    );

    @POST("member/job/update")
    @FormUrlEncoded
    Call<ResponseBody> updateMemberJob(@Header("Authorization") String authorization,
                                       @Field("id") int id,
                                       @Field("job_type_id") int jobTypeId
    );

    @GET("member/edit/client/{id}")
    Call<ResponseBody> fetchMemberClientDetails(@Header("Authorization") String authorization,
                                                @Path("id") int userId
    );

    @POST("member/update/client")
    @FormUrlEncoded
    Call<ResponseBody> updateClient(@Header("Authorization") String authorization,
                                    @Field("id") int id,
                                      @Field("res_addr_id") int residentialAddressId,
                                      @Field("business_addr_id") int businessAddressId,
                                      @Field("name") String name,
                                      @Field("email") String email,
                                      @Field("mobile") String mobile,
                                      @Field("pan") String pan,
                                      @Field("father_name") String fatherName,
                                      @Field("dob") String dob,
                                      @Field("gender") String gender,
                                      @Field("constitution") String constitution,
                                      @Field("trade_name") String tradeName,
                                      @Field("flat_no_r") String flatNo,
                                      @Field("village_r") String village,
                                      @Field("po_r") String po,
                                      @Field("ps_r") String ps,
                                      @Field("area_r") String area,
                                      @Field("district_r") String district,
                                      @Field("state_r") String state,
                                      @Field("pin_r") String pin,
                                      @Field("flat_no_b") String flatNoBusiness,
                                      @Field("village_b") String villageBusiness,
                                      @Field("po_b") String poBusiness,
                                      @Field("ps_b") String psBusiness,
                                      @Field("area_b") String areaBusiness,
                                      @Field("district_b") String districtBusiness,
                                      @Field("state_b") String stateBusiness,
                                      @Field("pin_b") String pinBusiness);

    @GET("member/close/job/list/{member_id}/{page}")
    Call<ResponseBody> fetchMemberCloseJobs( @Header("Authorization") String authorization,
                                             @Path("member_id") int memberId,
                                             @Path("page") int page
    );

    @GET("sp/wallet/history/{sp_id}")
    Call<ResponseBody> fetchSpWalletHistory(@Header("Authorization") String authorization,
                                          @Path("sp_id") int spId
    );

    @GET("member/search/jobs/{s_date}/{e_date}/{member_id}/{page}")
    Call<ResponseBody> fetchJobTransactions(@Header("Authorization") String authorization,
                                            @Path("s_date") String startDate,
                                            @Path("e_date") String endDate,
                                            @Path("member_id") int memberId,
                                            @Path("page") int page
    );

    @GET("member/wallet/history/{member_id}/{page}")
    Call<ResponseBody> fetchMemberWalletHistory(@Header("Authorization") String authorization,
                                                @Path("member_id") int memberId,
                                                @Path("page") int page
    );

    @POST("executive/login")
    @FormUrlEncoded
    Call<ResponseBody> checkExcutivelogin(@Field("email") String email,
                                            @Field("password") String password
    );

    @GET("executive/search/jobs/{s_date}/{e_date}/{member_id}/{page}")
    Call<ResponseBody> fetchExecutiveJobTransactions(@Header("Authorization") String authorization,
                                                    @Path("s_date") String startDate,
                                                    @Path("e_date") String endDate,
                                                    @Path("member_id") int memberId,
                                                    @Path("page") int page
    );

    @GET("executive/wallet/history/{executive_id}/{page}")
    Call<ResponseBody> fetchExecutiveWalletHistory(@Header("Authorization") String authorization,
                                                    @Path("executive_id") int memberId,
                                                    @Path("page") int page
    );

}
