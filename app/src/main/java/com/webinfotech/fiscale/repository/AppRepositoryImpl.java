package com.webinfotech.fiscale.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.fiscale.domain.models.ClientDetails;
import com.webinfotech.fiscale.domain.models.ClientDetailsWrapper;
import com.webinfotech.fiscale.domain.models.ClientListWrapper;
import com.webinfotech.fiscale.domain.models.ClientSearchWrapper;
import com.webinfotech.fiscale.domain.models.CloseJobsWrapper;
import com.webinfotech.fiscale.domain.models.CommonResponse;
import com.webinfotech.fiscale.domain.models.JobDescList;
import com.webinfotech.fiscale.domain.models.JobDescListWrapper;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsData;
import com.webinfotech.fiscale.domain.models.JobSearchDetailsDataWrapper;
import com.webinfotech.fiscale.domain.models.MemberClientDetailsWrapper;
import com.webinfotech.fiscale.domain.models.OpenJobsWrapper;
import com.webinfotech.fiscale.domain.models.SearchClientListWrapper;
import com.webinfotech.fiscale.domain.models.TransactionJobWrapper;
import com.webinfotech.fiscale.domain.models.UserInfoWrapper;
import com.webinfotech.fiscale.domain.models.WalletHistoryWrapper;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public UserInfoWrapper checkSPlogin(String email, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkSPlogin(email, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public JobDescListWrapper fetchJobDescList() {
        JobDescListWrapper jobDescListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getJobDescList();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    jobDescListWrapper = null;
                }else{
                    jobDescListWrapper = gson.fromJson(responseBody, JobDescListWrapper.class);
                }
            } else {
                jobDescListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            jobDescListWrapper = null;
        }
        return jobDescListWrapper;
    }

    public CommonResponse registerClient(String apiToken,
                                         String name,
                                         String email,
                                         String mobile,
                                         String pan,
                                         String fatherName,
                                         String dob,
                                         String gender,
                                         String constitution,
                                         String tradeName,
                                         int createdById,
                                         String flatNo,
                                         String village,
                                         String po,
                                         String ps,
                                         String area,
                                         String district,
                                         String state,
                                         String pin,
                                         String flatNoBusiness,
                                         String villageBusiness,
                                         String poBusiness,
                                         String psBusiness,
                                         String areaBusiness,
                                         String districtBusiness,
                                         String stateBusiness,
                                         String pinBusiness,
                                         ArrayList<Integer> jobIds
    ) {
        Log.e("LogMsg", "API Token: " + apiToken);
        Log.e("LogMsg", "User Id: " + createdById);
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerClient("Bearer " + apiToken, name, email, mobile, pan, fatherName, dob, gender, constitution, tradeName, createdById, flatNo, village, po, ps, area, district, state, pin, flatNoBusiness, villageBusiness, poBusiness, psBusiness, areaBusiness, districtBusiness, stateBusiness, pinBusiness, jobIds);

            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ClientListWrapper getClientList(String apiToken, int spId, int page) {
        ClientListWrapper clientListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getClientList("Bearer " + apiToken, spId, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    clientListWrapper = null;
                }else{
                    clientListWrapper = gson.fromJson(responseBody, ClientListWrapper.class);
                }
            } else {
                clientListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            clientListWrapper = null;
        }
        return clientListWrapper;
    }

    public SearchClientListWrapper searchClient(String apiToken, String searchKey) {
        SearchClientListWrapper searchClientListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.searchClient("Bearer " + apiToken, searchKey);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    searchClientListWrapper = null;
                }else{
                    searchClientListWrapper = gson.fromJson(responseBody, SearchClientListWrapper.class);
                }
            } else {
                searchClientListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            searchClientListWrapper = null;
        }
        return searchClientListWrapper;
    }

    public CommonResponse addExistingClientJobs(String apiToken, ArrayList<Integer> jobIds, int clientId, int spId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addExistingClientJob("Bearer " + apiToken, jobIds, clientId, spId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ClientSearchWrapper fetchClientWithJobs(String apiToken, String searchKey) {
        ClientSearchWrapper clientSearchWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.searchClientWithJobs("Bearer " + apiToken, searchKey);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    clientSearchWrapper = null;
                }else{
                    clientSearchWrapper = gson.fromJson(responseBody, ClientSearchWrapper.class);
                }
            } else {
                clientSearchWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            clientSearchWrapper = null;
        }
        return clientSearchWrapper;
    }

    public JobSearchDetailsDataWrapper searchClientJobs(String apiToken, String jobId) {
        JobSearchDetailsDataWrapper jobSearchDetailsDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.searchClientJobDetails("Bearer " + apiToken, jobId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    jobSearchDetailsDataWrapper = null;
                }else{
                    jobSearchDetailsDataWrapper = gson.fromJson(responseBody, JobSearchDetailsDataWrapper.class);
                }
            } else {
                jobSearchDetailsDataWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            jobSearchDetailsDataWrapper = null;
        }
        return jobSearchDetailsDataWrapper;
    }

    public CommonResponse addJobRemarks(String apiToken, int sp_id, int job_id, String remarks) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        Log.e("LogMsg", "JOb Id: " + job_id);
        try {
            Call<ResponseBody> add = mRepository.addJobRemarks("Bearer " + apiToken, sp_id, job_id, remarks);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ClientDetailsWrapper getClientDetails(String apiToken, int userId) {
        Log.e("LogMsg", "Client Id: " + userId);
        ClientDetailsWrapper clientDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchClientDetails("Bearer " + apiToken, userId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    clientDetailsWrapper = null;
                }else{
                    clientDetailsWrapper = gson.fromJson(responseBody, ClientDetailsWrapper.class);
                }
            } else {
                clientDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            clientDetailsWrapper = null;
        }
        return clientDetailsWrapper;
    }

    public OpenJobsWrapper fetchOpenJobs(String apiToken, int spId, int pageNo) {
        OpenJobsWrapper openJobsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchOpenJobs("Bearer " + apiToken, spId, pageNo);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    openJobsWrapper = null;
                }else{
                    openJobsWrapper = gson.fromJson(responseBody, OpenJobsWrapper.class);
                }
            } else {
                openJobsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            openJobsWrapper = null;
        }
        return openJobsWrapper;
    }

    public UserInfoWrapper checkMemberLogin(String email, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkMemberlogin(email, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public OpenJobsWrapper fetchMemberOpenJobs(String apiToken, int memberId, int pageNo) {
        OpenJobsWrapper openJobsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchMemberOpenJobs("Bearer " + apiToken, memberId, pageNo);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    openJobsWrapper = null;
                }else{
                    openJobsWrapper = gson.fromJson(responseBody, OpenJobsWrapper.class);
                }
            } else {
                openJobsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            openJobsWrapper = null;
        }
        return openJobsWrapper;
    }

    public JobSearchDetailsDataWrapper fetchMemberJobDetails(String apiToken, String jobId) {
        Log.e("LogMsg", "Job Id: " + jobId);
        JobSearchDetailsDataWrapper jobSearchDetailsDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchMemberJobDetails("Bearer " + apiToken, jobId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    jobSearchDetailsDataWrapper = null;
                }else{
                    jobSearchDetailsDataWrapper = gson.fromJson(responseBody, JobSearchDetailsDataWrapper.class);
                }
            } else {
                jobSearchDetailsDataWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            jobSearchDetailsDataWrapper = null;
        }
        return jobSearchDetailsDataWrapper;
    }

    public CommonResponse rejectJob(String apiToken, int jobId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.rejectJob("Bearer " + apiToken, jobId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ClientSearchWrapper searchMemberClient(String apiToken, String searchKey) {
        ClientSearchWrapper clientSearchWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.searchMemberClient("Bearer " + apiToken, searchKey);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    clientSearchWrapper = null;
                }else{
                    clientSearchWrapper = gson.fromJson(responseBody, ClientSearchWrapper.class);
                }
            } else {
                clientSearchWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            clientSearchWrapper = null;
        }
        return clientSearchWrapper;
    }

    public JobSearchDetailsDataWrapper getJobEditDetails(String apiToken, int jobId) {
        JobSearchDetailsDataWrapper jobSearchDetailsDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getJobEditDetails("Bearer " + apiToken, jobId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    jobSearchDetailsDataWrapper = null;
                }else{
                    jobSearchDetailsDataWrapper = gson.fromJson(responseBody, JobSearchDetailsDataWrapper.class);
                }
            } else {
                jobSearchDetailsDataWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            jobSearchDetailsDataWrapper = null;
        }
        return jobSearchDetailsDataWrapper;
    }

    public CommonResponse updateMember(String apiToken, int userId, int jobTypeId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.updateMemberJob("Bearer " + apiToken, userId, jobTypeId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public MemberClientDetailsWrapper getMemberClientDetails(String apiToken, int userId) {
        Log.e("LogMsg", "Client Id: " + userId);
        Log.e("LogMsg", "Api Token: " + apiToken);
        MemberClientDetailsWrapper clientDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchMemberClientDetails("Bearer " + apiToken, userId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse) {
                    clientDetailsWrapper = null;
                }else{
                    clientDetailsWrapper = gson.fromJson(responseBody, MemberClientDetailsWrapper.class);
                }
            } else {
                clientDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            clientDetailsWrapper = null;
        }
        return clientDetailsWrapper;
    }

    public CommonResponse updateClient(String apiToken,
                                         int id,
                                         int residentialAddressId,
                                         int businessAddressId,
                                         String name,
                                         String email,
                                         String mobile,
                                         String pan,
                                         String fatherName,
                                         String dob,
                                         String gender,
                                         String constitution,
                                         String tradeName,
                                         String flatNo,
                                         String village,
                                         String po,
                                         String ps,
                                         String area,
                                         String district,
                                         String state,
                                         String pin,
                                         String flatNoBusiness,
                                         String villageBusiness,
                                         String poBusiness,
                                         String psBusiness,
                                         String areaBusiness,
                                         String districtBusiness,
                                         String stateBusiness,
                                         String pinBusiness
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateClient("Bearer " + apiToken, id, residentialAddressId, businessAddressId, name, email, mobile, pan, fatherName, dob, gender, constitution, tradeName, flatNo, village, po, ps, area, district, state, pin, flatNoBusiness, villageBusiness, poBusiness, psBusiness, areaBusiness, districtBusiness, stateBusiness, pinBusiness);

            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CloseJobsWrapper fetchMemberCloseJobs(String apiToken, int memberId, int pageNo) {
        CloseJobsWrapper closeJobsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchMemberCloseJobs("Bearer " + apiToken, memberId, pageNo);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    closeJobsWrapper = null;
                }else{
                    closeJobsWrapper = gson.fromJson(responseBody, CloseJobsWrapper.class);
                }
            } else {
                closeJobsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            closeJobsWrapper = null;
        }
        return closeJobsWrapper;
    }

    public WalletHistoryWrapper fetchSpWalletHistory(int spId, String apiToken) {
        WalletHistoryWrapper walletHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchSpWalletHistory("Bearer " + apiToken, spId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletHistoryWrapper = null;
                }else{
                    walletHistoryWrapper = gson.fromJson(responseBody, WalletHistoryWrapper.class);
                }
            } else {
                walletHistoryWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            walletHistoryWrapper = null;
        }
        return walletHistoryWrapper;
    }

    public TransactionJobWrapper fetchTransactionJobs(String apiToken, String startDate, String endDate, int memberId, int page) {
        TransactionJobWrapper transactionJobWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchJobTransactions("Bearer " + apiToken, startDate, endDate, memberId, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    transactionJobWrapper = null;
                }else{
                    transactionJobWrapper = gson.fromJson(responseBody, TransactionJobWrapper.class);
                }
            } else {
                transactionJobWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            transactionJobWrapper = null;
        }
        return transactionJobWrapper;
    }

    public WalletHistoryWrapper fetchMemberWalletHistory(String apiToken, int memberId, int page) {
        WalletHistoryWrapper walletHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchMemberWalletHistory("Bearer " + apiToken, memberId, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletHistoryWrapper = null;
                }else{
                    walletHistoryWrapper = gson.fromJson(responseBody, WalletHistoryWrapper.class);
                }
            } else {
                walletHistoryWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            walletHistoryWrapper = null;
        }
        return walletHistoryWrapper;
    }

    public UserInfoWrapper checkExcutiveLogin(String email, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkExcutivelogin(email, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public TransactionJobWrapper fetchExecutiveTransactionJobs(String apiToken, String startDate, String endDate, int memberId, int page) {
        TransactionJobWrapper transactionJobWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchExecutiveJobTransactions("Bearer " + apiToken, startDate, endDate, memberId, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    transactionJobWrapper = null;
                }else{
                    transactionJobWrapper = gson.fromJson(responseBody, TransactionJobWrapper.class);
                }
            } else {
                transactionJobWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            transactionJobWrapper = null;
        }
        return transactionJobWrapper;
    }

    public WalletHistoryWrapper fetchExecutiveWalletHistory(String apiToken, int memberId, int page) {
        WalletHistoryWrapper walletHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchExecutiveWalletHistory("Bearer " + apiToken, memberId, page);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletHistoryWrapper = null;
                }else{
                    walletHistoryWrapper = gson.fromJson(responseBody, WalletHistoryWrapper.class);
                }
            } else {
                walletHistoryWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception: " + e.getMessage());
            walletHistoryWrapper = null;
        }
        return walletHistoryWrapper;
    }

}
